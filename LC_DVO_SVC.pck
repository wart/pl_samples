create or replace package LC_DVO_SVC is
  -- Author  : VLAD_ARTISCHEV
  -- Created : 02.03.2009 12:39:22
	
    type dvo_list IS TABLE OF dvo_obj INDEX BY BINARY_INTEGER;
    workflow dvo_list;
    DVO_ACT_MODE_NONE constant NUMBER:=0;
    DVO_ACT_MODE_ADD  constant NUMBER:=1;
		DVO_ACT_MODE_DEL  constant NUMBER:=2;
		DVO_ACT_MODE_CHNG constant NUMBER:=3;

    DVO_ACT_MODE_ACTIVE_MIN  CONSTANT NUMBER:=DVO_ACT_MODE_ADD;
		DVO_ACT_MODE_ACTIVE_MAX  CONSTANT NUMBER:=DVO_ACT_MODE_CHNG;


    DVO_ERROR_NONE          constant NUMBER:=0;
    DVO_ERROR_TRYOFF_OFFED  constant NUMBER:=-1;
    DVO_ERROR_TRYON_ONNED   constant NUMBER:=-2;
    DVO_ERROR_TRYON_STATION_FAIL constant NUMBER:=-3;		

    DEBUG_OUTPUT NUMBER:=0;
    DEBUG_OUTPUT_ITEM NUMBER:=0;


 function get_ats_TS(phon IN NUMBER) RETURN NUMBER;


 FUNCTION get_chng_svc(svcin NUMBER, svcout NUMBER) RETURN NUMBER;
 PROCEDURE print_workflow;
 PROCEDURE init_workflow;
 PROCEDURE fill_user_data(usr1 NUMBER,phon NUMBER,atsmod NUMBER);

 FUNCTION chk_usr_svc(usr1 NUMBER,phon NUMBER,svc NUMBER) RETURN NUMBER;
	 
 FUNCTION get_final_count  RETURN NUMBER;
 FUNCTION get_active_count RETURN NUMBER; 
 FUNCTION get_active_countb(without IN NUMBER) RETURN NUMBER; 
 FUNCTION create_zayavka(usr1 IN NUMBER,phone IN NUMBER, dvolst IN VARCHAR2) RETURN VARCHAR2;

 PROCEDURE CHECK_TON_NABOR( TONID IN NUMBER);
PROCEDURE Debug_PRN(msg IN VARCHAR2);

 function get_ats_GSTS(phon IN NUMBER) RETURN NUMBER;
 

 FUNCTION create_dvo_on(phon IN VARCHAR2,pwd IN VARCHAR2,dvo IN NUMBER) RETURN NUMBER;
 FUNCTION create_dvo_off(phon IN VARCHAR2,pwd IN VARCHAR2,dvo IN NUMBER) RETURN NUMBER;
 FUNCTION chk_dvo_access(dvo IN NUMBER, phon IN NUMBER) RETURN NUMBER;
 
 
 PROCEDURE NA_setchanges(user_id in number,dev_id in number,dvo_acts IN VARCHAR2);
 function NA_setchanges_single(usr_id in number,
                                         dv_id  in number,
                                         dvoid in number,
                                         stt    in number) return number;
Procedure NA_Zayvk_Handle;                                         





Cursor user_dvo_on_phone (usrid varchar2,devid varchar2) IS
  select *
          from (select lcw.id,
                 lcw.name,
                 nvl(ss.ison, 0) as ison,
                 case nvl(nmn.service_id, 0) when 0 then 0 else 1 end
--                 (if nvl(nmn.service_id, 0)<>0 then 1 else 0 end if)
                 --nvl(nmn.service_id, 0) 
                 as srvid,
                 nvl(udw.state, 0) ch_state,
                 to_char(nvl(udw.date_post, sysdate), 'dd.mm.yyyy') as ch_date
            from t_LC_DVO_SVC lcw
            left join (SELECT w1.id, COUNT(1) as ison
                        FROM t_LC_DVO_SVC w1, O1.T_STATION_COND sc1
                       where sc1.DEV_ID = (0 + devid)
                         AND DATE_BEGIN =
                             (SELECT MAX(DATE_BEGIN)
                                FROM O1.T_STATION_COND sc
                               WHERE sc.DEV_ID = sc1.DEV_ID)
                         AND DATE_BEGIN <= SYSDATE
                         AND STATION_COND_ID IN
                             (SELECT SV.STATION_COND_ID
                                FROM O1.T_SVC_ATS_CONDITION SV,
                                     t_LC_DVO_SVC           W
                               WHERE w.id = w1.id
                                 AND (EXISTS
                                      (SELECT 1
                                         FROM O1.T_VOLUME V, O1.T_ATS A
                                        WHERE (UPPER(A.COMMENTS) LIKE
                                              '%���%' OR UPPER(A.COMMENTS) LIKE
                                              '%C��%')
                                          AND V.DEV_ID = sc1.DEV_ID
                                          AND V.DEPT_ID = A.ATS_ID
                                          AND W.R_SVC_GTS = SV.SVC_ID)))
                       group by sc1.DEV_ID, w1.id) ss on ss.id =
                                                         lcw.id
            LEFT JOIN o1.t_services nmn ON nmn.dev_id = (0 + devid)
                                       AND nmn.user_id = (0 + usrid)
                                       AND nmn.svc_id in
                                           (lcw.c_svc_gtsnm,
                                            lcw.c_svc_stsnm,
                                            lcw.c_svc_sts,
                                            lcw.c_svc_gts)
                                       AND nmn.date_end IS NULL
            left join USERS_DVO_WANTS udw on udw.user_id = (0 + usrid)
                                         and udw.dev_id = (0 + devid)
                                         and lcw.id = udw.dvo_id
           order by lcw.id)
         where ison = 1;


end lc_dvo_svc;
/
create or replace package body LC_DVO_SVC is



PROCEDURE Debug_PRN(msg IN VARCHAR2)IS
BEGIN
    IF DEBUG_OUTPUT=1 THEN 
	    dbms_output.put_line(msg);
	  END IF;
END;



  function get_ats_TS(phon IN NUMBER) RETURN NUMBER is
  ret NUMBER;
  begin
      SELECT COUNT(1) INTO ret FROM o1.T_ATS a,o1.t_volume v 
			                        WHERE v.dept_id=a.ats_id 
															  AND v.dev_id=phon 
																AND upper(a.comments) like '%���%';
      IF ret=1 THEN
          ret:=1;
      ELSE
          SELECT COUNT(1) INTO ret FROM o1.T_ATS a,o1.t_volume v 
					                        WHERE v.dept_id=a.ats_id 
																	  AND v.dev_id=phon 
																		AND upper(a.comments) like '%���%';
          IF ret=1 THEN
              ret:=2;
          ELSE
              ret:=-1;
          END IF;
      END IF;
      RETURN ret;
  END;

  PROCEDURE init_workflow IS
	BEGIN
	  workflow.delete;
		
		FOR x IN (SELECT * FROM t_lc_dvo_svc) LOOP
		  workflow(x.id):=NEW dvo_obj(x.id);
		END LOOP;
	END;

  PROCEDURE print_workflow IS
	ri PLS_INTEGER:=workflow.first;
	BEGIN
	  IF ri IS NULL THEN
		  Debug_PRN('workflow is emply.');
			ELSE
			Debug_PRN('workflow items count: '||workflow.count);
		END IF;
  	LOOP
		   EXIT WHEN ri IS NULL;
			 Debug_PRN('row index: '||ri);
			 workflow(ri).print_data;
   	  ri:=workflow.next(ri);
	  END LOOP;
	
	END;

  PROCEDURE fill_user_data(usr1 NUMBER,phon NUMBER,atsmod NUMBER) IS
	ri PLS_INTEGER:=workflow.first;
	BEGIN
  	LOOP
		   EXIT WHEN ri IS NULL;
			 workflow(ri).chk_ats_and_user_present(usr1,phon,atsmod);
   	  ri:=workflow.next(ri);
	  END LOOP;
		END;

 FUNCTION get_final_count RETURN NUMBER IS
	BEGIN
 		RETURN get_active_count;
	END;


 FUNCTION get_active_count RETURN NUMBER IS
 	ri PLS_INTEGER:=workflow.first;
	rcnt NUMBER:=0;
	BEGIN
  	LOOP
		   EXIT WHEN ri IS NULL;
       IF workflow(ri).isactive THEN
         rcnt:=rcnt+1;
       END IF;
   	   ri:=workflow.next(ri);
	  END LOOP;
		RETURN rcnt;
	END;
 
 FUNCTION get_active_countb(without IN NUMBER) RETURN NUMBER IS
  	ri PLS_INTEGER:=workflow.first;
	rcnt NUMBER:=0;
	BEGIN
  	LOOP
		   EXIT WHEN ri IS NULL;
			  IF workflow(ri).id<>without THEN
       IF workflow(ri).isactive THEN
         rcnt:=rcnt+1;
       END IF;END IF;
   	   ri:=workflow.next(ri);
	  END LOOP;
		RETURN rcnt;
	END;
 


  FUNCTION chk_usr_svc(usr1 NUMBER,phon NUMBER,svc NUMBER) RETURN NUMBER IS
	ret NUMBER:=NULL;
	BEGIN
	 IF svc IS NOT NULL THEN
    select ts.service_id 
		  INTO ret
      from dual LEFT JOIN o1.t_services ts ON ts.dev_id=phon 
					 AND ts.user_id=usr1 
					 AND ts.svc_id=svc
					 AND ts.date_end IS NULL 
					 WHERE rownum<2;
    END IF;
		RETURN ret;
	END;


 FUNCTION get_chng_svc(svcin NUMBER, svcout NUMBER) RETURN NUMBER IS
 r NUMBER;
 BEGIN
   select r_svc INTO r from dual LEFT JOIN t_lc_dvo_svc_chng t ON svc_in=svcin AND svc_out=svcout AND rownum=1;
	 RETURN r;
 END;

 PROCEDURE DBG_PRN( NAM IN VARCHAR2)is
 BEGIN
   IF DEBUG_OUTPUT=1 THEN 
   dbms_output.put_line(NAM);
   END IF;
 END;
 
 
 PROCEDURE DBG_PRN_LS( a IN lib_string.Table_String_Type, NAM IN VARCHAR2 )IS
 BEGIN
   IF DEBUG_OUTPUT=1 THEN 
  	 dbms_output.put_line('--'||NAM||'('||a.count||'):');
 	   IF a.count>0 THEN
       for cd in 1..a.count LOOP
         dbms_output.put_line(a(cd));
       END LOOP;
	   END IF;
   END IF;
 END;
 

 PROCEDURE abon_action_fill(dvo_acts IN VARCHAR2) IS
   actsplit lib_string.Table_String_Type;
   dvosplit lib_string.Table_String_Type; 
 BEGIN
  actsplit.delete; 
 
  lib_string.Parser(dvo_acts,';',actsplit);
	DBG_PRN_LS(actsplit,'actsplit');
	IF actsplit.count>0 THEN
 
	dvosplit.delete;
	IF actsplit(1) IS NOT NULL THEN
  	lib_string.Parser(actsplit(1),',',dvosplit);
			DBG_PRN_LS(dvosplit,'dvo to add');
      for cd in 1..dvosplit.count LOOP
	  	IF dvosplit(cd) IS NOT NULL THEN
		  IF workflow.exists(dvosplit(cd)) THEN
			workflow(dvosplit(cd)).set_action(DVO_ACT_MODE_ADD);
			NULL;
      end if;
      end if;			
    end loop;
   
	END IF;
 
		END IF;
	
	IF actsplit.count>1 THEN
 
	  dvosplit.delete;
		IF actsplit(2) IS NOT NULL THEN
  	lib_string.Parser(actsplit(2),',',dvosplit);
		DBG_PRN_LS(dvosplit,'dvo to del');
    for cd in 1..dvosplit.count LOOP
		IF dvosplit(cd) IS NOT NULL THEN		
		  IF workflow.exists(dvosplit(cd)) THEN
			workflow(dvosplit(cd)).set_action(DVO_ACT_MODE_DEL);
      end if;
      end if;			
    end loop;
   
	END IF;
 
	END IF;	
  
 END;

 FUNCTION call_create_dvo_zayvka(usr1 IN NUMBER,phon IN NUMBER,atsmod IN NUMBER) RETURN VARCHAR2 IS
	ri PLS_INTEGER;
	r NUMBER; ret VARCHAR2(2000):=NULL;
	BEGIN
    SAVEPOINT DVOEXEC;
		
		Debug_PRN('sozdanie zayavlenii:');
		

  	FOR dvomode IN DVO_ACT_MODE_ACTIVE_MIN .. DVO_ACT_MODE_ACTIVE_MAX LOOP
      ri:=workflow.first;
			Debug_PRN('  DVO MOde: '||dvomode);
  	  LOOP
        EXIT WHEN ri IS NULL;
        IF workflow(ri).action=dvomode THEN
  		    r:=workflow(ri).create_zayvlenie(usr1,phon,atsmod);
    			Debug_PRN('  DVO: '||ri||'   RET:'||r);					
	  	    IF ret IS NOT NULL THEN ret:=ret||',';END IF;
		  	  ret:=ret||ri||':'||r;					
	  		  IF r<=0 THEN
					  ROLLBACK TO DVOEXEC;
						EXIT;
  			  END IF;
         END IF;
   	      ri:=workflow.next(ri);
	    END LOOP;
    END LOOP;	
		Debug_PRN('FIN RET: '||ret);	
		Debug_PRN('sozdanie zayavlenii-----end');	
  	RETURN ret;
 END;
  


 FUNCTION create_zayavka(usr1 IN NUMBER,phone IN NUMBER, dvolst IN VARCHAR2) RETURN VARCHAR2 IS
 atsmode NUMBER;
 final_count NUMBER; ret VARCHAR2(2000);
 BEGIN
      Debug_PRN('usr1: '||usr1);	
			Debug_PRN('phone: '||phone);	
			Debug_PRN('dvolst: '||dvolst);	
			Debug_PRN('');	
 
     init_workflow;
		 
     atsmode:=get_ats_TS(phone);
		 
		 Debug_PRN('atsmode: '||atsmode);

	   fill_user_data(usr1,phone,atsmode);
	   abon_action_fill(dvolst);
	   final_count:=get_final_count;
   
		 Debug_PRN('final_count: '||final_count);
		 
     CHECK_TON_NABOR(1);
    
     ret:=call_create_dvo_zayvka(usr1,phone,atsmode);
		 
		 print_workflow();
		 
     RETURN ret;
 END;

 PROCEDURE CHECK_TON_NABOR( TONID IN NUMBER) IS
 CNT_NO_TON NUMBER;
 
 BEGIN                      
   dbms_output.put_line('TONID: ' || TONID);
   IF workflow.exists(TONID) THEN
     dbms_output.put_line('TONID: ' || TONID || ' exists');
     CNT_NO_TON:=get_active_countb(TONID);
 	   dbms_output.put_line('CNT_NO_TON: ' || CNT_NO_TON);
     IF CNT_NO_TON>0 THEN
		   IF workflow(TONID).isactive THEN
	      IF workflow(TONID).serv_id IS NOT NULL THEN
			     IF workflow(TONID).isnomoney =0 THEN
				     workflow(TONID).isnomoney:=1;
				     workflow(TONID).action:=DVO_ACT_MODE_CHNG;
						 workflow(TONID).error:=DVO_ERROR_NONE;
						 
			     ELSE
				     workflow(TONID).action:=DVO_ACT_MODE_NONE;
				   END IF;						 
				ELSE
			     workflow(TONID).isnomoney:=1;
				END IF;
  		ELSE
	      workflow(TONID).isnomoney:=1;
	      workflow(TONID).action:=DVO_ACT_MODE_ADD;
			END IF;
	   ELSE
		   IF workflow(TONID).isactive THEN
			   IF workflow(TONID).serv_id IS NOT NULL THEN
			     IF workflow(TONID).isnomoney =1 THEN
				     workflow(TONID).isnomoney:=0;
				     workflow(TONID).action:=DVO_ACT_MODE_CHNG;
						 workflow(TONID).error:=DVO_ERROR_NONE;
			     ELSE
				     workflow(TONID).action:=DVO_ACT_MODE_NONE;
				   END IF;
         ELSE		
			     workflow(TONID).isnomoney:=0;
				 END IF;
			 END IF;		 
	   END IF;
   END IF;
 END;

FUNCTION create_dvo_on(phon IN VARCHAR2,pwd IN VARCHAR2,dvo IN NUMBER) RETURN NUMBER IS
 usr NUMBER;
 rsvc NUMBER;
 chk NUMBER;
 res NUMBER;
 atsmode NUMBER; 

BEGIN
--1a,2p,3k
    res:=0;
    usr := abon_tool.user_start(phon,pwd);
    dbms_output.put_line('usr_id: '||usr);
    IF usr>0 THEN
		    atsmode:=get_ats_GSTS(phon);
        dbms_output.put_line('atsmode: '||atsmode);		 
        IF atsmode>0 AND atsmode<3 THEN
            SELECT COUNT(1) INTO chk 
              FROM o1.T_STATION_COND 
             WHERE dev_id=phon 
               AND date_begin=(SELECT max(date_begin) 
							                   FROM o1.T_STATION_COND 
																WHERE dev_id=phon)
               AND date_begin<=SYSDATE
               AND station_cond_id IN (SELECT st_cond 
							                           FROM t_lc_dvo_sts_cond 
																				where ID=dvo
																			);
            dbms_output.put_line('station chk (1 ok): '||chk);
            IF chk=1 THEN
						    IF atsmode=1 THEN
                    SELECT s.r_svc_gts INTO rsvc FROM dual LEFT JOIN t_lc_dvo_svc s ON id=dvo WHERE rownum<2;							
							  ELSE
							      IF atsmode=1 THEN
                        SELECT s.r_svc_sts INTO rsvc FROM dual LEFT JOIN t_lc_dvo_svc s ON id=dvo WHERE rownum<2;							
									  ELSE
									      res:=-203;
										END IF;
						    END IF;										
                dbms_output.put_line('rsvc: '||rsvc);				
		            IF rsvc IS NOT NULL THEN
                    select COUNT(1) INTO chk
                      from o1.N_CONST_SVC nc,
                           o1.t_services ts
                     where nc.svc_id=rsvc
                       and ts.svc_id=nc.const_svc_id
                       and nc.isdefault='Y'
                       AND ts.dev_id=phon
                       AND ts.date_end IS NULL;
									 dbms_output.put_line('chk svc (0-ok): '||chk);
									 IF chk=0 THEN
                       res:=zayvl_api.get_applic_ust_post(usr,rsvc,phon);
									 ELSE
								    res:=-205;
									 END IF;
                ELSE
								    res:=-204;
                END IF;
						ELSE
                res:=-202;
						END IF;				    
        ELSE
            res:=-201;
        END IF;
    ELSE
        res:=-2;
    END IF;
    RETURN res;
END;
function get_ats_GSTS(phon IN NUMBER) RETURN NUMBER is
ret NUMBER;

begin
    SELECT COUNT(1) INTO ret FROM o1.T_ATS a,o1.t_volume v WHERE v.dept_id=a.ats_id AND v.dev_id=phon AND upper(a.comments) like '%���%';
    IF ret=1 THEN
     NULL;
    ELSE
        SELECT COUNT(1) INTO ret FROM o1.T_ATS a,o1.t_volume v WHERE v.dept_id=a.ats_id AND v.dev_id=phon AND upper(a.comments) like '%���%';
        IF ret=1 THEN
          ret:=2;
        ELSE
          ret:=-1;
        END IF;
    END IF;
    RETURN ret;
end ;





FUNCTION create_dvo_off(phon IN VARCHAR2,pwd IN VARCHAR2,dvo IN NUMBER) RETURN NUMBER IS
 usr NUMBER;
 csvc NUMBER;
 res NUMBER;
BEGIN
--1a,2p,3k
    res:=0;
    usr := abon_tool.user_start(phon,pwd);
    dbms_output.put_line('usr_id: '||usr);
    IF usr>0 THEN
		  BEGIN
          select ts.svc_id INTO csvc
            from o1.t_services ts,t_lc_dvo_svc d
           where (ts.svc_id=d.c_svc_gts OR ts.svc_id=d.c_svc_sts)
             AND ts.dev_id=phon
		  			 AND ts.user_id=usr
             AND ts.date_end IS NULL AND d.id=dvo AND rownum<2;
			EXCEPTION
			WHEN OTHERS THEN
			 	 csvc:=NULL;
			eND;	 
      dbms_output.put_line('csvc: '||csvc);
      IF csvc IS NOT NULL THEN
          res:=zayvl_api.get_applic_kill_post(usr,695,csvc,phon);
      ELSE
          res:=-301;
      END IF;
    ELSE
        res:=-2;
    END IF;
    RETURN res;
END;


FUNCTION chk_dvo_access(dvo IN NUMBER, phon IN NUMBER) RETURN NUMBER IS
res NUMBER;
BEGIN

SELECT COUNT(1)
  INTO res
  FROM O1.T_STATION_COND
 WHERE DEV_ID = PHON
   AND DATE_BEGIN =
       (SELECT MAX(DATE_BEGIN) FROM O1.T_STATION_COND WHERE DEV_ID = PHON)
   AND DATE_BEGIN <= SYSDATE
      
   AND STATION_COND_ID IN
       (SELECT
        
         SV.STATION_COND_ID
          FROM O1.T_SVC_ATS_CONDITION SV, t_LC_DVO_SVC W
         WHERE 2 = 2
           AND dvo = W.ID
           AND (EXISTS (SELECT 1
                          FROM O1.T_VOLUME V, O1.T_ATS A
                         WHERE V.DEV_ID = DEV_ID
                           AND V.DEPT_ID = A.ATS_ID
                           AND( UPPER(A.COMMENTS) LIKE '%���%' OR UPPER(A.COMMENTS) LIKE '%C��%' )
                           AND W.R_SVC_GTS = SV.SVC_ID)));
  RETURN res;
END;
---------------------------------------------------------------------------------
 PROCEDURE NA_setchanges(user_id  in number,
                         dev_id   in number,
                         dvo_acts IN VARCHAR2) IS
   actsplit lib_string.Table_String_Type;
   dvosplit lib_string.Table_String_Type;
   did      number;
   inprms varchar2(2048);
 BEGIN
   actsplit.delete;
 
   lib_string.Parser(dvo_acts, ';', actsplit);
   DBG_PRN_LS(actsplit, 'actsplit');
 
   IF actsplit.count > 0 THEN
     dvosplit.delete;
     IF actsplit(1) IS NOT NULL THEN
       lib_string.Parser(actsplit(1), ',', dvosplit);
       DBG_PRN_LS(dvosplit, 'dvo to add');
       
       inprms:='���������: '||chr(13);
       for cd in 1 .. dvosplit.count LOOP
         IF dvosplit(cd) IS NOT NULL THEN
           did := dvosplit(cd) + 0;
           inprms:=inprms||abon_tool.getWRKlstItem(0,did)||': ';
           did := NA_setchanges_single(user_id, dev_id, dvosplit(cd), 2);
           if did<0 then inprms:=inprms||'������'||chr(13); else inprms:=inprms||'Ok'||chr(13);end if;
         end if;
       end loop;
     END IF;
   END IF;
   IF actsplit.count > 1 THEN
     dvosplit.delete;
     IF actsplit(2) IS NOT NULL THEN
       lib_string.Parser(actsplit(2), ',', dvosplit);
       DBG_PRN_LS(dvosplit, 'dvo to del');
       inprms:=inprms||'����������: '||chr(13);       
       for cd in 1 .. dvosplit.count LOOP
         IF dvosplit(cd) IS NOT NULL THEN
           did := dvosplit(cd) + 0;
           inprms:=inprms||abon_tool.getWRKlstItem(0,did)||': ';           
           did := NA_setchanges_single(user_id, dev_id, dvosplit(cd), 1);
           if did<0 then inprms:=inprms||'������'||chr(13); else inprms:=inprms||'Ok'||chr(13);end if;           
         end if;
       end loop;
     END IF;
   END IF;
   
        abon_tool.Register_action(user_id,dev_id,5,dvo_acts,inprms);
   
 END;
------------------------------------------------------------------------------------------------------
function NA_setchanges_single(usr_id in number,
                                         dv_id  in number,
                                         dvoid in number,
                                         stt    in number) return number IS
BEGIN

  if stt = 1 or stt = 2 then
    for x in (select id, ison,service_id as srvid,ch_state
                from user_dvo 
               where id = dvoid
               and dev_id=dv_id
               and user_id=usr_id) loop
------------------------------------------------------------------------------------------------------
      DBG_PRN('----------------------');
      DBG_PRN('arg usr_id: '||usr_id);
      DBG_PRN('arg dv_id : '||dv_id);
      DBG_PRN('arg dvoid : '||dvoid);
      DBG_PRN('arg stt   : '||stt);                  
      DBG_PRN('x.id       ' || x.id);
      DBG_PRN('x.ison     ' || x.ison);
      DBG_PRN('x.srvid    ' || x.srvid);
      DBG_PRN('x.ch_state ' || x.ch_state);
------------------------------------------------------------------------------------------------------
      if x.ison = 1 then
        if stt = 1 then 
                                                DBG_PRN(' --- needs OFF');
          if x.srvid <> 0 then         
                                                DBG_PRN('     when ON');          
            case x.ch_state
------------------------------------------------------------------------------------------------------
              when 0 then
                                                DBG_PRN('   * when old req empty');              
                                                DBG_PRN('ON: 1 | STT:  1 | SRV:  1 | XST: 0  ');
                                                DBG_PRN('   ACTION: insert');                                                 
                insert into USERS_DVO_WANTS(USER_ID,DEV_ID,DVO_ID,STATE) 
                values(usr_id,dv_id,dvoid,stt);
                                                DBG_PRN('   RESULT: '||SQL%Rowcount);
------------------------------------------------------------------------------------------------------
              when 1 then
                                                DBG_PRN('   * when old req need OFF');              
                                                DBG_PRN('ON: 1 | STT:  1 | SRV:  1 | XST: 1  ');
                                                DBG_PRN('   ACTION: update');                                                 
                update USERS_DVO_WANTS udw 
                   set date_post=sysdate 
                 where udw.user_id = usr_id 
                   and udw.dev_id = dv_id 
                   and udw.dvo_id=dvoid 
                   and udw.date_handle is null;
                                                DBG_PRN('   RESULT: '||SQL%Rowcount);
------------------------------------------------------------------------------------------------------
              when 2 then
                                                DBG_PRN('   * when old req need ON');              
                                                DBG_PRN('ON: 1 | STT:  1 | SRV:  1 | XST: 2  ');
                                                DBG_PRN('   ACTION: update');                                                
                update USERS_DVO_WANTS udw 
                   set STATE=stt 
                 where udw.user_id = usr_id 
                   and udw.dev_id = dv_id 
                   and udw.dvo_id=dvoid 
                   and udw.date_handle is null;
                                                DBG_PRN('   RESULT: '||SQL%Rowcount);
------------------------------------------------------------------------------------------------------
              else
                                                DBG_PRN('   * when error');  
                                                DBG_PRN('ON: 1 | STT:  1 | SRV:  1 | XST: '||x.ch_state);
                                                DBG_PRN('   ACTION: nothing');    
                                                DBG_PRN('   RESULT: none');
                                                                                            
                return - 2;
------------------------------------------------------------------------------------------------------
            end case;
          else
                                                DBG_PRN('     when OFF');             
            case x.ch_state
------------------------------------------------------------------------------------------------------
              when 0 then
                                                DBG_PRN('   * when old req empty');              
                                                DBG_PRN('ON: 1 | STT:  1 | SRV:  0 | XST: 0  ');              
                                                DBG_PRN('   ACTION: all ok nothing to do..');
------------------------------------------------------------------------------------------------------
              when 1 then
                                                DBG_PRN('   * when old req need OFF');              
                                                DBG_PRN('ON: 1 | STT:  1 | SRV:  0 | XST: 1  ');              
                                                DBG_PRN('   ACTION: delete (overhead)');  
                delete from USERS_DVO_WANTS udw
                 where udw.user_id = usr_id 
                   and udw.dev_id = dv_id 
                   and udw.dvo_id=dvoid 
                   and udw.date_handle is null;
                                                DBG_PRN('   RESULT: '||SQL%Rowcount);
------------------------------------------------------------------------------------------------------
              when 2 then
                                                DBG_PRN('   * when old req need ON');              
                                                DBG_PRN('ON: 1 | STT:  1 | SRV:  0 | XST: 2  ');              
                                                DBG_PRN('   ACTION: delete (overhead)');  
                delete from USERS_DVO_WANTS udw
                 where udw.user_id = usr_id 
                   and udw.dev_id = dv_id 
                   and udw.dvo_id=dvoid 
                   and udw.date_handle is null;
                                                DBG_PRN('   RESULT: '||SQL%Rowcount);
------------------------------------------------------------------------------------------------------
              else
                                                DBG_PRN('   * when error');  
                                                DBG_PRN('ON: 1 | STT:  1 | SRV:  0 | XST: '||x.ch_state);
                                                DBG_PRN('   ACTION: nothing');    
                                                DBG_PRN('   RESULT: none');
                return - 2;
------------------------------------------------------------------------------------------------------
            end case;
          end if;
        end if;
        if stt = 2 then
                                                DBG_PRN(' --- needs ON');
          if x.srvid <> 0 then
                                                DBG_PRN('     when ON'); 
            case x.ch_state
------------------------------------------------------------------------------------------------------
              when 0 then
                                                DBG_PRN('   * when old req empty');              
                                                DBG_PRN('ON: 1 | STT:  2 | SRV:  1 | XST: 0  ');              
                                                DBG_PRN('   ACTION: all ok nothing to do..');              
------------------------------------------------------------------------------------------------------
              when 1 then
                                                DBG_PRN('   * when old need OFF');              
                                                DBG_PRN('ON: 1 | STT:  2 | SRV:  1 | XST: 1  ');
                                                DBG_PRN('   ACTION: delete (overhead offs)');                                                  
                delete from USERS_DVO_WANTS udw
                 where udw.user_id = usr_id 
                   and udw.dev_id = dv_id 
                   and udw.dvo_id=dvoid 
                   and udw.date_handle is null;
                                                DBG_PRN('   RESULT: '||SQL%Rowcount);
------------------------------------------------------------------------------------------------------
              when 2 then  
                                                DBG_PRN('   * when old need ON');              
                                                DBG_PRN('ON: 1 | STT:  2 | SRV:  1 | XST: 2  ');
                                                DBG_PRN('   ACTION: delete (overhead ons)');  
                delete from USERS_DVO_WANTS udw
                 where udw.user_id = usr_id
                   and udw.dev_id = dv_id
                   and udw.dvo_id=dvoid 
                   and udw.date_handle is null;
                                                DBG_PRN('   RESULT: '||SQL%Rowcount);
------------------------------------------------------------------------------------------------------
              else
                                                DBG_PRN('   * when error');  
                                                DBG_PRN('ON: 1 | STT:  2 | SRV:  1 | XST: '||x.ch_state);
                                                DBG_PRN('   ACTION: nothing');    
                                                DBG_PRN('   RESULT: none');
                return - 2;
            end case;
 ------------------------------------------------------------------------------------------------------
          else
                                                DBG_PRN('     when OFF');              
            case x.ch_state
------------------------------------------------------------------------------------------------------
              when 0 then
                                                DBG_PRN('   * when old req empty');              
                                                DBG_PRN('ON: 1 | STT:  2 | SRV:  0 | XST: 0  ');      
                insert into USERS_DVO_WANTS(USER_ID,DEV_ID,DVO_ID,STATE) 
                values(usr_id,dv_id,dvoid,stt);
                                                DBG_PRN('   RESULT: '||SQL%Rowcount);                
------------------------------------------------------------------------------------------------------
              when 1 then
                                                DBG_PRN('   *when old req need OFF');                
                                                DBG_PRN('ON: 1 | STT:  2 | SRV:  0 | XST: 1  ');               
                                                DBG_PRN('   ACTION: update');
                update USERS_DVO_WANTS udw 
                   set STATE=stt 
                 where udw.user_id = usr_id 
                   and udw.dev_id = dv_id 
                   and udw.dvo_id=dvoid 
                   and udw.date_handle is null;
                                                DBG_PRN('   RESULT: '||SQL%Rowcount);                   
------------------------------------------------------------------------------------------------------
              when 2 then
                                                DBG_PRN('   *when old req need ON');
                                                DBG_PRN('ON: 1 | STT:  2 | SRV:  0 | XST: 2  ');               
                                                DBG_PRN('   ACTION: update');
                update USERS_DVO_WANTS udw 
                   set date_post=sysdate 
                 where udw.user_id = usr_id 
                   and udw.dev_id = dv_id 
                   and udw.dvo_id=dvoid 
                   and udw.date_handle is null;
                                                DBG_PRN('   RESULT: '||SQL%Rowcount);                   
------------------------------------------------------------------------------------------------------
              else
                                                DBG_PRN('   * when error');  
                                                DBG_PRN('ON: 1 | STT:  2 | SRV:  0 | XST: '||x.ch_state);
                                                DBG_PRN('   ACTION: nothing');    
                                                DBG_PRN('   RESULT: none');              
                return - 2;
------------------------------------------------------------------------------------------------------
            end case;
          end if;
        end if;
        return 0;      
      else
------------------------------------------------------------------------------------------------------
        if x.ch_state<>0 then
          delete from USERS_DVO_WANTS udw 
           where udw.user_id = usr_id 
             and udw.dev_id = dv_id 
             and udw.dvo_id=dvoid 
             and udw.date_handle is null;
          commit;
          return - 1;
        end if;
------------------------------------------------------------------------------------------------------
      end if;
    end loop;
    return -4;
  end if;
  return -3;
END;
------------------------------------------------------------------------------------------------------
Procedure NA_Zayvk_Handle is
  atsmode number;
begin
  for device in (select user_id, dev_id
                   from users_dvo_wants t
                  where date_handle is null
                  group by user_id, dev_id) loop
    atsmode := get_ats_TS(device.dev_id);
    Debug_PRN('USR: ' || device.user_id);
    Debug_PRN('DEV: ' || device.dev_id);
    Debug_PRN('atsmode: ' || atsmode);
    Debug_PRN('----------------------');
    init_workflow;
    fill_user_data(device.user_id, device.dev_id, atsmode);
    for dvo in (select dvo_id, state, date_post
                  from users_dvo_wants tt
                 where date_handle is null
                   and tt.user_id = device.user_id
                   and tt.dev_id = device.dev_id) loop
      Debug_PRN('DVO:' || dvo.dvo_id || '   STATE: ' || dvo.state);
      IF workflow.exists(dvo.dvo_id) THEN
        if dvo.state = 1 then
          workflow(dvo.dvo_id) .set_action(DVO_ACT_MODE_DEL);
        else
          workflow(dvo.dvo_id) .set_action(DVO_ACT_MODE_ADD);
        end if;
      end if;
    end loop;
    Debug_PRN('FCount: ' || get_final_count);
    CHECK_TON_NABOR(1);
    Debug_PRN('RET: ' || call_create_dvo_zayvka(device.user_id,
                                                device.dev_id,
                                                atsmode));
    Debug_PRN('======================');
     print_workflow();
    --  update users_dvo_wants t set date_handle=sysdate where t.user_id=device.user_id and t.dev_id=device.dev_id;
  end loop;
end;
------------------------------------------------------------------------------------------------------




begin
  -- Initialization
  NULL;
  --<Statement>;
end LC_DVO_SVC;
/
