create or replace type dvo_obj as object
(
  -- Author  : VLAD_ARTISCHEV
  -- Created : 02.03.2009 11:18:08
  -- Purpose : 

  id         NUMBER,
	dNAME      VARCHAR2(64),
	rsvc_gts   NUMBER,
	rsvc_sts   NUMBER,
	csvc_gts   NUMBER,
	csvc_sts   NUMBER,
	csvc_gtsnm NUMBER,
	csvc_stsnm NUMBER,

	issts      NUMBER,
	isnomoney  NUMBER,
	station_ok NUMBER,

	serv_id    NUMBER, -- ���� �� ��� �� ������� � ��������
	action     NUMBER, -- �������� 0 ��� ��������� 1 ���������� 2 �������� 
	                   -- 3 ����� � �������� �� ����������
										 -- 4 ����� ��������
  error      NUMBER,
--	ats_state  NUMBER,
  







  -- Attributes
--  <Attribute> <Datatype>,
  
  -- Member functions and procedures
	CONSTRUCTOR FUNCTION dvo_obj(id1 NUMBER) RETURN SELF AS RESULT,

	MEMBER PROCEDURE chk_ats_and_user_present(usr1 NUMBER,phon NUMBER,atsmod NUMBER),
	MEMBER PROCEDURE print_data,
	MEMBER PROCEDURE set_action(ac NUMBER),	
	MEMBER FUNCTION isvalid RETURN BOOLEAN,
	MEMBER FUNCTION isactive RETURN BOOLEAN,
	MEMBER FUNCTION actcsvc RETURN NUMBER,
	MEMBER FUNCTION actrsvc RETURN NUMBER,	
	
	MEMBER FUNCTION create_zayvlenie(usr1 NUMBER,phon NUMBER,atsmod NUMBER)RETURN NUMBER
)
/
create or replace type body dvo_obj is
	CONSTRUCTOR FUNCTION dvo_obj(id1  NUMBER)
	RETURN SELF AS RESULT 
	IS
	BEGIN
	    SELF.id:=id1;
		  SELECT t.NAME,
			       t.r_svc_gts,
						 t.c_svc_gts,
						 t.r_svc_sts,
						 t.c_svc_sts,
						 t.c_svc_gtsnm,
						 t.c_svc_stsnm
						 INTO self.dNAME,
                  self.rsvc_gts,
                  self.csvc_gts,
                  self.rsvc_sts,									
                  self.csvc_sts,
                  self.csvc_gtsnm,
                  self.csvc_stsnm						 
		    FROM t_lc_dvo_svc t
		   WHERE t.id=id1;
			 self.action:=LC_DVO_SVC.DVO_ACT_MODE_NONE;
	     self.issts:=0;
	     self.isnomoney:=0;
			 SELF.error:=LC_DVO_SVC.DVO_ERROR_NONE;
			 RETURN;
	END;
  
	MEMBER FUNCTION actcsvc RETURN NUMBER IS
	ret NUMBER;
	BEGIN
	  IF self.isnomoney=1 THEN
		  IF self.issts=1 THEN ret:=  csvc_stsnm; ELSE ret:=  csvc_gtsnm; END IF;
		ELSE
		  IF self.issts=1 THEN ret:= csvc_sts  ; ELSE ret:=  csvc_gts  ; END IF;
		END IF;
		LC_DVO_SVC.Debug_PRN('const svc: id: '||id||
            ' sts: '||issts||
            ' nomoney: '||isnomoney||
            ' ret: '||ret);
		RETURN ret;
	END;
	
		MEMBER FUNCTION actrsvc RETURN NUMBER IS
	ret NUMBER;
	BEGIN

		  IF self.issts=1 THEN ret:=  rsvc_sts; ELSE ret:=  rsvc_gts; END IF;
			
		IF LC_DVO_SVC.DEBUG_OUTPUT_ITEM=1 THEN dbms_output.put_line(' raz svc: id: '||self.id||' sts: '||self.issts||' ret: '||ret);END IF;		
		RETURN ret;
	END;
	

	MEMBER PROCEDURE print_data	IS
	BEGIN
	 IF lc_dvo_svc.DEBUG_OUTPUT=1 THEN
	    dbms_output.put_line('------------------------------');	
	    dbms_output.put_line('id         : '||self.id        );
	    dbms_output.put_line('dNAME      : '||self.dNAME     );
	    dbms_output.put_line('rsvc_gts   : '||self.rsvc_gts  );
	    dbms_output.put_line('rsvc_sts   : '||self.rsvc_sts  );
	    dbms_output.put_line('csvc_gts   : '||self.csvc_gts  );
	    dbms_output.put_line('csvc_sts   : '||self.csvc_sts  );
	    dbms_output.put_line('csvc_gtsnm : '||self.csvc_gtsnm);
    	dbms_output.put_line('csvc_stsnm : '||self.csvc_stsnm);
    	dbms_output.put_line('issts      : '||self.issts     );
	    dbms_output.put_line('isnomoney  : '||self.isnomoney );
	    dbms_output.put_line('station_ok : '||self.station_ok);
	    dbms_output.put_line('serv_id    : '||self.serv_id   );
	    dbms_output.put_line('action     : '||self.action    );
	    dbms_output.put_line('error      : '||self.error     );

	    dbms_output.put_line('');	
			END IF;
	END;
	
	MEMBER PROCEDURE chk_ats_and_user_present(usr1 NUMBER,phon NUMBER,atsmod NUMBER) IS
	BEGIN
	
SELECT COUNT(1)
  INTO SELF.STATION_OK
  FROM O1.T_STATION_COND
 WHERE DEV_ID = PHON
   AND DATE_BEGIN =
       (SELECT MAX(DATE_BEGIN) FROM O1.T_STATION_COND WHERE DEV_ID = PHON)
   AND DATE_BEGIN <= SYSDATE
      
   AND STATION_COND_ID IN
       (SELECT
        
         SV.STATION_COND_ID
          FROM O1.T_SVC_ATS_CONDITION SV, t_LC_DVO_SVC W
         WHERE 2 = 2
           AND SELF.ID = W.ID
           AND (EXISTS (SELECT 1
                          FROM O1.T_VOLUME V, O1.T_ATS A
                         WHERE V.DEV_ID = DEV_ID
                           AND V.DEPT_ID = A.ATS_ID
                           AND( UPPER(A.COMMENTS) LIKE '%���%' OR UPPER(A.COMMENTS) LIKE '%C��%' )
                           AND W.R_SVC_GTS = SV.SVC_ID)));
	
	
	/*
	            SELECT COUNT(1) INTO SELF.station_ok 
              FROM o1.T_STATION_COND 
             WHERE dev_id=phon 
               AND date_begin=(SELECT max(date_begin) 
							                   FROM o1.T_STATION_COND 
																WHERE dev_id=phon)
               AND date_begin<=SYSDATE
               AND station_cond_id IN (SELECT st_cond 
							                           FROM t_lc_dvo_sts_cond 
																				where ID=self.id
																			);
																			/**/
																			
																			
     isnomoney:=0;
		   IF atsmod=1 THEN
	       issts:=0;	 		 
		   ELSE			 
	       issts:=1;		 
  		 END IF;			 
			 
     SELF.serv_id:= LC_DVO_SVC.chk_usr_svc(usr1,phon,self.csvc_gts);
		 IF SELF.serv_id IS NULL THEN
  		 SELF.serv_id:= LC_DVO_SVC.chk_usr_svc(usr1,phon,self.csvc_sts);
  		 IF SELF.serv_id IS NULL THEN
    		 SELF.serv_id:= LC_DVO_SVC.chk_usr_svc(usr1,phon,self.csvc_gtsnm);
  		   IF SELF.serv_id IS NULL THEN
    		   SELF.serv_id:= LC_DVO_SVC.chk_usr_svc(usr1,phon,self.csvc_stsnm);
					 IF SELF.serv_id IS NOT NULL THEN
				     isnomoney:=1;
			       issts:=1;
 	         END IF;
			   ELSE
				   isnomoney:=1;
 	       END IF;
			 ELSE
			   issts:=1;
 	     END IF;
 	   END IF;
	END;
	
	MEMBER PROCEDURE set_action(ac NUMBER) IS
	BEGIN
  LC_DVO_SVC.Debug_PRN('SET ACTION: '||ac||'  ON DVO: '||SELF.id);
	    IF ac=LC_DVO_SVC.DVO_ACT_MODE_ADD THEN
			    self.action:=LC_DVO_SVC.DVO_ACT_MODE_ADD;			
			    IF self.serv_id IS NOT NULL THEN
			        SELF.error:=LC_DVO_SVC.DVO_ERROR_TRYON_ONNED;			
			    END IF;
					IF self.station_ok=0 THEN
					   SELF.error:=LC_DVO_SVC.DVO_ERROR_TRYON_STATION_FAIL;
          END IF;					
		  ELSE
	        IF ac=LC_DVO_SVC.DVO_ACT_MODE_DEL THEN
			        IF self.serv_id IS  NULL THEN
			            SELF.error:=LC_DVO_SVC.DVO_ERROR_TRYOFF_OFFED;			
                  else
                  self.action:=LC_DVO_SVC.DVO_ACT_MODE_DEL;
			        END IF;
		      ELSE
		          NULL;		
		      END IF;
		  END IF;
      LC_DVO_SVC.Debug_PRN('RESULT: '||self.action ||' ERR: '|| SELF.error);
	END;
	
	MEMBER FUNCTION isvalid RETURN BOOLEAN IS
	BEGIN
	  RETURN SELF.error=LC_DVO_SVC.DVO_ERROR_NONE;
	END;
	
  MEMBER FUNCTION isactive RETURN BOOLEAN IS
	r BOOLEAN;
	BEGIN
	r:=(SELF.serv_id IS     NULL AND  self.action=LC_DVO_SVC.DVO_ACT_MODE_ADD)
		       OR
		       (SELF.serv_id IS NOT NULL AND 
					               (self.action=LC_DVO_SVC.DVO_ACT_MODE_NONE OR self.action=LC_DVO_SVC.DVO_ACT_MODE_CHNG
												 OR (self.action=LC_DVO_SVC.DVO_ACT_MODE_ADD AND SELF.error<>LC_DVO_SVC.DVO_ERROR_TRYON_STATION_FAIL)
												 )
					 );
				 	IF LC_DVO_SVC.DEBUG_OUTPUT_ITEM=1 THEN 
						  dbms_output.put_line('DVO #'||self.id||' isactive: ');
							IF r THEN dbms_output.put_line('true');	ELSE dbms_output.put_line('false');	END IF;
							
          END IF;	 
					 RETURN r; 
	END;
	
  MEMBER FUNCTION create_zayvlenie(usr1 NUMBER,phon NUMBER,atsmod NUMBER)RETURN NUMBER IS
	 od dvo_obj; rsvc NUMBER;
	BEGIN

	  IF self.action=LC_DVO_SVC.DVO_ACT_MODE_ADD THEN
	    IF SELF.error=LC_DVO_SVC.DVO_ERROR_NONE THEN		--  ��������� �� ����������
			
			  rsvc:=zayvl_api.get_applic_ust_post(usr1,SELF.actrsvc,phon);
				IF rsvc<0 THEN
				  rsvc:=rsvc-400;	
				END IF;
				RETURN rsvc;
			ELSE  -- �����
			   RETURN -400+SELF.error;
			END IF;
		ELSE
  	  IF self.action=LC_DVO_SVC.DVO_ACT_MODE_DEL THEN				
	      IF SELF.error=LC_DVO_SVC.DVO_ERROR_NONE THEN		--  ��������� �� ��������
			    rsvc:= zayvl_api.get_applic_kill_post(usr1,695,SELF.actcsvc,phon);
					IF rsvc<0 THEN
					  rsvc:=rsvc-410;	
					END IF;
					RETURN rsvc;
			  ELSE  -- �����
			    RETURN -400+SELF.error;
			  END IF;
			ELSE
  	    IF self.action=LC_DVO_SVC.DVO_ACT_MODE_CHNG THEN		
	        IF SELF.error=LC_DVO_SVC.DVO_ERROR_NONE THEN		--  ��������� �� ���������
	          od:=NEW dvo_obj(self.id);			
						od.chk_ats_and_user_present(usr1,phon,atsmod);
						rsvc:=lc_dvo_svc.get_chng_svc(od.actcsvc,SELF.actcsvc);
						IF rsvc IS NOT NULL THEN
						  rsvc:= zayvl_api.get_applic_smena_75(usr1,rsvc,od.actcsvc,SELF.actcsvc,phon);
			        IF rsvc<0 THEN
						    rsvc:=rsvc-420;	
							END IF;
							RETURN rsvc;
						ELSE  -- �����
			        RETURN -400;
			      END IF;
			    ELSE  -- �����
			      RETURN -400+SELF.error;
			    END IF;
			  ELSE
			      NULL;
  			END IF;
			END IF;
	  END IF;
		
		RETURN 0;
	END;


end;
/
