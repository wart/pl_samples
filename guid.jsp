create or replace and compile java source named guid as
//import java.net.InetAddress;
//import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class GUID {
  private static StringBuffer IPAddressSegment;
  private static SecureRandom prng;
  static {
    try {
      // Get IPAddress Segment
      IPAddressSegment = new StringBuffer("322307324");
//      InetAddress addr = InetAddress.getLocalHost();
//      StringBuffer strTemp = new StringBuffer();
//      byte[] ipaddr = addr.getAddress();
//      for (int i = 0; i < ipaddr.length; i++) {
//        Byte b = new Byte(ipaddr[i]);
//        strTemp = new StringBuffer(Integer.toHexString(b.intValue() & 0x000000ff));
//        while (strTemp.length() < 2) {
//          strTemp.insert(0, '0');
//        }
//        IPAddressSegment.append(strTemp);
//      }
      //Get Random Segment Algoritm
      prng = SecureRandom.getInstance("SHA1PRNG");
//    } catch (UnknownHostException ex) { ex.printStackTrace();
    } catch (NoSuchAlgorithmException nsae) {  nsae.printStackTrace(); }
  }

  public static final String getUID()
  {
         StringBuffer strRetVal = new StringBuffer(IPAddressSegment.toString());
         StringBuffer strTemp = new StringBuffer(Long.toHexString(System.currentTimeMillis()));
         while (strTemp.length() < 12) {strTemp.insert(0, "0");}
         strRetVal.append(strTemp);
         
         strTemp = new StringBuffer(Integer.toHexString(prng.nextInt()));
         while (strTemp.length() < 8) {strTemp.insert(0, "0");}
         strRetVal.append(strTemp.substring(4,8));
         
         
         strTemp = new StringBuffer(Integer.toHexString(System.identityHashCode((Object) new GUID())));
         while (strTemp.length() < 8) {strTemp.insert(0, "0");}
         
         strRetVal.append(strTemp);
         return strRetVal.toString().toUpperCase();
  }
  
  public static final String getUIDB()
  {
         return getUIDC(getUID());
  }
  
  public static final String getUIDC(String j)
  {
         return j.substring(0,8)+"-"+j.substring(8,12)+"-"+j.substring(12,16)+"-"+j.substring(16,20)+"-"+j.substring(20);
  }
}
/
