CREATE OR REPLACE PACKAGE inet_bronb IS

  -- Author  : VLAD_ARTISCHEV
  -- Created : 04.03.2009 14:56:10

  -- old LK utils
  FUNCTION get_usr_tp(usr1 NUMBER, phn NUMBER, oldsvc IN OUT NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_tp_bron_svc(tp VARCHAR2) RETURN NUMBER;
  FUNCTION get_tp_main_svc(tp NUMBER) RETURN NUMBER;
  FUNCTION get_usr_bron_tp(usr NUMBER, phn NUMBER, br_svc IN OUT NUMBER)
    RETURN NUMBER;
  -- old LK actors
  FUNCTION zayavka_brone(phn VARCHAR2, pwd VARCHAR2, mon NUMBER)
    RETURN VARCHAR2;
  FUNCTION zayavka_bron_off(phn VARCHAR2, pwd VARCHAR2) RETURN VARCHAR2;

  -- MY LK utils
  FUNCTION get_usr_tpnlc(usr1   NUMBER,
                         oldsvc IN OUT NUMBER,
                         devid  IN OUT NUMBER) RETURN VARCHAR2;
  FUNCTION get_usr_bron_tpb(usr    NUMBER,
                            phn    IN OUT NUMBER,
                            br_svc IN OUT NUMBER) RETURN NUMBER;
  -- My LK actors
  FUNCTION zayavka_broneb(usr1 NUMBER, mon NUMBER, ret2 OUT NUMBER)
    RETURN NUMBER;
  FUNCTION zayavka_bron_offb(usr1 VARCHAR2, ret2 OUT NUMBER) RETURN NUMBER;
  FUNCTION bronstate(usr1 NUMBER) RETURN NUMBER;
  FUNCTION bronstateb(usr1 NUMBER, dev NUMBER) RETURN NUMBER;

  -- Common handler job procedure
  PROCEDURE handle_bron_zayavka;

  -- debug logging enabling to dbms_output
  PROCEDURE enabledebugout;
  FUNCTION bron_on_off(a_account  IN VARCHAR2,
                       a_stat     IN NUMBER, --- 0 vkl    1 ����
                       ds         DATE,
                       de         DATE,
                       a_debugout IN NUMBER := 0) RETURN VARCHAR2;

  -- ������������ IP-TV
  FUNCTION bron_on_off_iptv(p_account    VARCHAR2,
                            p_status     NUMBER,
                            p_date_start DATE,
                            p_date_end   DATE) RETURN VARCHAR2;

  -- trigger utils

  FUNCTION chksvcsect(svcid NUMBER, sct NUMBER) RETURN NUMBER;

  PROCEDURE fillsvcflags(svcid   NUMBER,
                         userid  NUMBER,
                         numlist NUMBER,
                         act     OUT NUMBER,
                         acc     OUT VARCHAR2,
                         ds      OUT DATE,
                         de      OUT DATE);

END inet_bronb;
/
CREATE OR REPLACE PACKAGE BODY inet_bronb IS

  inoo        NUMBER := 0;
  debugenable NUMBER := 0;

  PROCEDURE enabledebugout IS
  BEGIN
    debugenable := 1;
  END;

  ----------------------------------------------------------------------------

  PROCEDURE log(s VARCHAR2, ino NUMBER) IS
    ts VARCHAR2(1024) := '';
    j  NUMBER := 0;
  BEGIN
    IF debugenable = 1 THEN
      IF ino = 1 THEN
        FOR j IN 1 .. inoo LOOP
          ts := ts || '  ';
        END LOOP;
        inoo := inoo + 1;
        dbms_output.put_line(ts || s);
      ELSE
        inoo := inoo - 1;
        FOR j IN 1 .. inoo LOOP
          ts := ts || '  ';
        END LOOP;
        dbms_output.put_line(ts || s);
      END IF;
    END IF;
  END;

  ----------------------------------------------------------------------------

  FUNCTION get_usr_tp(usr1 NUMBER, phn NUMBER, oldsvc IN OUT NUMBER)
    RETURN VARCHAR2 IS
    utp VARCHAR2(32);
  BEGIN
    log('get_usr_tp(): usr1=' || usr1 || '  phn=' || phn, 1);
    SELECT r.tfp_name, r.svc_id
      INTO utp, oldsvc
      FROM dual
      LEFT JOIN (SELECT tr.tfp_name, tc.svc_id
                   FROM msn.oren_tfp_cross tc,
                        o1.t_svc_ref       r,
                        msn.oren_tfp_ref   tr,
                        o1.t_services      ts
                  WHERE tr.iscorp = 'N'
                    AND tr.tfp_id = tc.tfp_id
                    AND tc.group_id = 5
                    AND tc.svc_id = r.svc_id
                    AND tc.svc_id = ts.svc_id
                    AND ts.user_id = usr1
                    AND ts.date_end IS NULL
                    AND ts.dev_id = phn) r
        ON 1 = 1
     WHERE rownum = 1;
    log('get_usr_tp() ret : tp:' || utp || '  oldsvc: ' || oldsvc, 0);
    RETURN utp;
  END;

  ----------------------------------------------------------------------------

  FUNCTION get_usr_tpnlc(usr1   NUMBER,
                         oldsvc IN OUT NUMBER,
                         devid  IN OUT NUMBER) RETURN VARCHAR2 IS
    utp VARCHAR2(32);
  BEGIN
    log('get_usr_tpNLC(): usr1=' || usr1, 1);
    SELECT r.tfp_name, r.svc_id, r.dev_id
      INTO utp, oldsvc, devid
      FROM dual
      LEFT JOIN (SELECT tr.tfp_name, tc.svc_id, ts.dev_id
                   FROM msn.oren_tfp_cross tc,
                        o1.t_svc_ref       r,
                        msn.oren_tfp_ref   tr,
                        o1.t_services      ts
                  WHERE tr.iscorp = 'N'
                    AND tr.tfp_id = tc.tfp_id
                    AND tc.group_id = 5
                    AND tc.svc_id = r.svc_id
                    AND tc.svc_id = ts.svc_id
                    AND ts.user_id = usr1
                    AND ts.date_end IS NULL) r
        ON 1 = 1
     WHERE rownum = 1;
    log('get_usr_tpNLC() ret : tp=' || utp || '  oldsvc=' || oldsvc ||
        'devid=' || devid,
        0);
    RETURN utp;
  END;

  ----------------------------------------------------------------------------

  FUNCTION get_tp_bron_svc(tp VARCHAR2) RETURN NUMBER IS
    ret NUMBER;
  BEGIN
    log('get_tp_bron_svc():tp=' || tp, 1);
    SELECT svc_id
      INTO ret
      FROM dual
      LEFT JOIN (SELECT r.svc_id
                   FROM msn.oren_tfp_cross tc,
                        o1.t_svc_ref       r,
                        msn.oren_tfp_ref   tr
                  WHERE tr.iscorp = 'N'
                    AND tr.tfp_id = tc.tfp_id
                    AND tc.group_id = 5
                    AND tc.svc_id = r.svc_id
                    AND tr.tfp_name = tp
                    AND r.name LIKE '%����%')
        ON 1 = 1
     WHERE rownum = 1;
    log('get_tp_bron_svc() ret : svc_id=' || ret, 0);
    RETURN ret;
  END;

  ----------------------------------------------------------------------------

  FUNCTION get_usr_bron_tp(usr NUMBER, phn NUMBER, br_svc IN OUT NUMBER)
    RETURN NUMBER IS
    ret NUMBER;
  BEGIN
    log('get_usr_bron_tp(): usr=' || usr || '  phn=' || phn, 1);
    SELECT tfp_id, svc_id
      INTO ret, br_svc
      FROM dual
      LEFT JOIN (SELECT tfp_id, svc_id
                   FROM msn.oren_tfp_cross o
                  WHERE o.svc_id IN
                        (SELECT svc_id
                           FROM o1.t_svc_ref
                          WHERE svc_id IN (SELECT svc_id
                                             FROM o1.t_services
                                            WHERE user_id = usr
                                              AND date_end IS NULL
                                              AND dev_id = phn)
                            AND NAME LIKE '%����%'))
        ON 1 = 1
     WHERE rownum = 1;
    log('get_usr_bron_tp() ret : tfp_id=' || ret || '  br_svc=' || br_svc,
        0);
    RETURN ret;
  END;

  ----------------------------------------------------------------------------

  FUNCTION get_usr_bron_tpb(usr    NUMBER,
                            phn    IN OUT NUMBER,
                            br_svc IN OUT NUMBER) RETURN NUMBER IS
    ret NUMBER;
  BEGIN
    log('get_usr_bron_tpB(): usr=' || usr, 1);
    SELECT tfp_id, svc_id, dev_id
      INTO ret, br_svc, phn
      FROM dual
      LEFT JOIN (SELECT o.tfp_id, o.svc_id, s.dev_id
                   FROM msn.oren_tfp_cross o,
                        o1.t_svc_ref       r,
                        o1.t_services      s
                  WHERE o.svc_id = r.svc_id
                    AND o.svc_id = s.svc_id
                    AND s.date_end IS NULL
                    AND r.name LIKE '%����%'
                    AND s.user_id = usr)
        ON 1 = 1
     WHERE rownum = 1;
    log('get_usr_bron_tpB() ret : tfp_id=' || ret || '  br_svc=' || br_svc,
        0);
    RETURN ret;
  END;

  ----------------------------------------------------------------------------

  FUNCTION bronstate(usr1 NUMBER) RETURN NUMBER IS
    res   NUMBER;
    os    NUMBER;
    devid NUMBER;
  BEGIN
    log('BronState(): usr=' || usr1, 1);
    res := get_usr_bron_tpb(usr1, devid, os);
    IF res IS NULL THEN
      res := 0;
    ELSE
      res := 1;
    END IF;
    log('BronState() ret : res=' || res, 0);
    RETURN res;
  END;

  ----------------------------------------------------------------------------

  FUNCTION bronstateb(usr1 NUMBER, dev NUMBER) RETURN NUMBER IS
    res NUMBER;
  BEGIN
    SELECT tfp_id
      INTO res
      FROM dual
      LEFT JOIN (SELECT tfp_id, svc_id
                   FROM msn.oren_tfp_cross o
                  WHERE o.svc_id IN
                        (SELECT svc_id
                           FROM o1.t_svc_ref
                          WHERE svc_id IN (SELECT svc_id
                                             FROM o1.t_services
                                            WHERE user_id = usr1
                                              AND date_end IS NULL
                                              AND dev_id = dev)
                            AND NAME LIKE '%����%'))
        ON 1 = 1
     WHERE rownum = 1;
    IF res IS NULL THEN
      res := 0;
    ELSE
      res := 1;
    END IF;
    RETURN res;
  END;

  ----------------------------------------------------------------------------

  FUNCTION get_tp_main_svc(tp NUMBER) RETURN NUMBER IS
    ret NUMBER;
  BEGIN
    log('get_tp_main_svc(): tp=' || tp, 1);
    SELECT svc_id
      INTO ret
      FROM dual
      LEFT JOIN (SELECT tsr.svc_id
                   FROM msn.oren_tfp_cross otc,
                        msn.oren_tfp_ref   otr,
                        o1.t_svc_ref       tsr
                  WHERE otc.group_id = 5
                    AND otc.tfp_id = otr.tfp_id
                    AND otc.svc_id = tsr.svc_id
                    AND tsr.isconst = 'Y'
                    AND otc.svc_id IN
                        (SELECT svc_id
                           FROM o1.t_svc_layer_bunch
                          WHERE id_layer_svc = 5926589644
                            AND id_cell_svc = 5926590210)
                    AND otr.tfp_id = tp)
        ON 1 = 1
     WHERE rownum = 1;
    log('get_tp_main_svc() ret : ret=' || ret, 0);
    RETURN ret;
  END;

  ----------------------------------------------------------------------------

  FUNCTION zayavka_brone(phn VARCHAR2, pwd VARCHAR2, mon NUMBER)
    RETURN VARCHAR2 IS
    usr1   NUMBER;
    tpbsbc NUMBER;
    oldsvc NUMBER := NULL;
    tp     VARCHAR2(32);
    ret    VARCHAR2(1024);
    ret2   VARCHAR2(1024);
  BEGIN
    log('zayavka_brone(): phn=' || phn || ' pwd=' || pwd || '  mon=' || mon,
        1);
    usr1 := abon_tool.user_start(phn, pwd);
    IF usr1 > 0 THEN
      tp := get_usr_tp(usr1, phn, oldsvc);
      IF tp IS NOT NULL AND oldsvc IS NOT NULL THEN
        tpbsbc := get_tp_bron_svc(tp);
        IF tpbsbc IS NOT NULL THEN
          ret := zayvl_api.get_applic_smena_75(usr1,
                                               620,
                                               oldsvc,
                                               tpbsbc,
                                               phn);
          IF ret > 0 THEN
            -- ret2   := zayvl_api.get_applic_ust_post_18(usr1,3451381608,mon);
            log('ret2: ' || ret2, 1);
            --          IF ret2>0 THEN                                   ret    := ret||':'||ret2;
            --          ELSE                       ret:=ret||':'||(-510+ret2); END IF; 
          ELSE
            ret := -500 + ret;
          END IF;
        ELSE
          ret := -502;
        END IF;
      ELSE
        ret := -501;
      END IF;
    ELSE
      ret := usr1 - 100;
    END IF;
    log('zayavka_brone() ret : ret=' || ret, 0);
    RETURN ret;
  END;

  ----------------------------------------------------------------------------

  FUNCTION zayavka_broneb(usr1 NUMBER, mon NUMBER, ret2 OUT NUMBER)
    RETURN NUMBER IS
    tp     VARCHAR2(32);
    tpbsbc NUMBER;
    ret    NUMBER;
    devid  NUMBER;
    oldsvc NUMBER := NULL;
  BEGIN
    log('zayavka_brone(): usr1=' || usr1 || '  mon=' || mon, 1);
    IF usr1 > 0 THEN
      tp := get_usr_tpnlc(usr1, oldsvc, devid);
      IF tp IS NOT NULL AND oldsvc IS NOT NULL AND devid IS NOT NULL THEN
        tpbsbc := get_tp_bron_svc(tp);
        IF tpbsbc IS NOT NULL THEN
          ret := zayvl_api.get_applic_smena_75(usr1,
                                               620,
                                               oldsvc,
                                               tpbsbc,
                                               devid);
          IF ret > 0 THEN
            NULL;
            --              ret2:=zayvl_api.get_applic_ust_post_18(usr1,3451381608,mon);
            --             IF ret2<=0 THEN ret2:=-510+ret2;           END IF;
          ELSE
            ret := -500 + ret;
          END IF;
        ELSE
          ret := -502;
        END IF;
      ELSE
        ret := -501;
      END IF;
    ELSE
      ret := usr1 - 100;
    END IF;
    log('zayavka_broneb() ret : ret=' || ret, 0);
    abon_tool.register_action(usr1,
                              NULL,
                              3,
                              abon_tool.getwrklstitem(3, mon),
                              abon_tool.getineterror(ret) || ':' ||
                              abon_tool.getineterror(ret2));
    abon_tool.register_offers(usr1, 106);
    RETURN ret;
  END;

  ----------------------------------------------------------------------------

  FUNCTION zayavka_bron_off(phn VARCHAR2, pwd VARCHAR2) RETURN VARCHAR2 IS
    usr1   NUMBER;
    tp     NUMBER;
    tpbsbc NUMBER;
    oldsvc NUMBER := NULL;
    ret    VARCHAR2(1024);
    --ret2 VARCHAR2(1024);
  BEGIN
    log('zayavka_bron_off(): phn=' || phn || '  pwd=' || pwd, 1);
    usr1 := abon_tool.user_start(phn, pwd);
    IF usr1 > 0 THEN
      tp := get_usr_bron_tp(usr1, phn, oldsvc);
      IF tp IS NOT NULL AND oldsvc IS NOT NULL THEN
        tpbsbc := get_tp_main_svc(tp);
        IF tpbsbc IS NOT NULL THEN
          ret := zayvl_api.get_applic_smena_75(usr1,
                                               620,
                                               oldsvc,
                                               tpbsbc,
                                               phn);
          IF ret > 0 THEN
            NULL;
            --             ret2:=zayvl_api.get_applic_kill_post_11(usr1,25134236,3451359007);
            --             IF ret2>0 THEN          ret:= ret||':'||ret2;
            --             ELSE                    ret:= ret||':'||(-610+ret2);  END IF;
          
          ELSE
            ret := -600;
          END IF;
        ELSE
          ret := -602;
        END IF;
      ELSE
        ret := -601;
      END IF;
    ELSE
      ret := usr1 - 100;
    END IF;
    log('zayavka_bron_off() ret : ret=' || ret, 0);
    RETURN ret;
  END;

  ----------------------------------------------------------------------------

  FUNCTION zayavka_bron_offb(usr1 VARCHAR2, ret2 OUT NUMBER) RETURN NUMBER IS
    phn    NUMBER;
    tp     NUMBER;
    tpbsbc NUMBER;
    oldsvc NUMBER := NULL;
    ret    NUMBER; --ret2   NUMBER;
  BEGIN
    log('zayavka_bron_offb(): usr=' || usr1, 1);
    --     usr1:=abon_tool.user_start(phn,pwd);
    IF usr1 > 0 THEN
      tp := get_usr_bron_tpb(usr1, phn, oldsvc);
      IF tp IS NOT NULL AND oldsvc IS NOT NULL THEN
        tpbsbc := get_tp_main_svc(tp);
        IF tpbsbc IS NOT NULL THEN
          ret := zayvl_api.get_applic_smena_75(usr1,
                                               620,
                                               oldsvc,
                                               tpbsbc,
                                               phn);
          IF ret > 0 THEN
            NULL;
            --             ret2:=zayvl_api.get_applic_kill_post_11(usr1,25134236,3451359007);
            --             IF ret2<=0 THEN         ret2:= -610+ret;              END IF;
          ELSE
            ret := -600;
          END IF;
        ELSE
          ret := -602;
        END IF;
      ELSE
        ret := -601;
      END IF;
    ELSE
      ret := usr1 - 100;
    END IF;
    log('zayavka_bron_off() ret : ret=' || ret, 0);
    abon_tool.register_action(usr1,
                              NULL,
                              4,
                              NULL,
                              abon_tool.getineterror(ret) || ':' ||
                              abon_tool.getineterror(ret2));
    dbms_output.put_line('tp:' || tp);
    dbms_output.put_line('tpbsbc:' || tpbsbc);
    dbms_output.put_line('oldsvc:' || oldsvc);
    dbms_output.put_line('phn:' || phn);
  
    RETURN ret;
  END;

  ----------------------------------------------------------------------------

  PROCEDURE handle_bron_zayavka IS
    rr VARCHAR2(1024);
    rs NUMBER;
  BEGIN
    FOR x IN (SELECT lk.rowid rd,
                     usr.account,
                     CASE tip
                       WHEN 4 THEN
                        1
                       WHEN 10 THEN
                        0
                       ELSE
                        2
                     END AS a_stas,
                     CASE
                       WHEN lk.sd < SYSDATE THEN
                        SYSDATE + 0.001
                       ELSE
                        lk.sd
                     END AS sd
                FROM msn.oren_lichniy_kabinet lk, o1.t_users usr
               WHERE lk.tip IN (4, 10)
                 AND lk.startip IS NULL
                 AND lk.raz_svc_id = 620
                 AND lk.rez = 'OK'
                 AND lk.user_id = usr.user_id
                 AND lk.action_nar IS NOT NULL) LOOP
      IF x.a_stas = 0 THEN
        rr := bron_on_off(x.account, x.a_stas, NULL, x.sd, 1);
      ELSE
        rr := bron_on_off(x.account, x.a_stas, x.sd, NULL, 1);
      END IF;
      IF rr = '1' THEN
        rs := 1;
      ELSE
        rs := -100;
      END IF;
      UPDATE msn.oren_lichniy_kabinet l
         SET l.startip = rs
       WHERE l.rowid = x.rd;
    END LOOP;
  END;

  ----------------------------------------------------------------------------

  PROCEDURE fillsvcflags(svcid   NUMBER,
                         userid  NUMBER,
                         numlist NUMBER,
                         act     OUT NUMBER,
                         acc     OUT VARCHAR2,
                         ds      OUT DATE,
                         de      OUT DATE) IS
    dd DATE;
  BEGIN
    BEGIN
      ------------------------- 
      SELECT u.account INTO acc FROM o1.t_users u WHERE u.user_id = userid;
      ------------------------- 
      SELECT oo.date_action, add_months(oo.date_action, 3)
        INTO ds, dd
        FROM o1.n_order_life oo
       WHERE oo.num_list = numlist
         AND oo.type_action_id = 1;
      ------------------------- 
      SELECT flag, decode(flag, 1, dd, ds)
        INTO act, de
        FROM t_bron_svc_flags
       WHERE svc_id = svcid;
      ------------------------- 
    EXCEPTION
      WHEN OTHERS THEN
        raise_application_error(-20001,
                                'fillSVCFlags(' || svcid || ',' || userid || ',' ||
                                numlist || ')');
    END;
  END;

  ----------------------------------------------------------------------------

  FUNCTION chksvcsect(svcid NUMBER, sct NUMBER) RETURN NUMBER IS
    ret NUMBER;
  BEGIN
    SELECT COUNT(1)
      INTO ret
      FROM t_bron_svc_flags
     WHERE svc_id = svcid
       AND sect = sct;
    RETURN ret;
  END;

  ----------------------------------------------------------------------------

  FUNCTION bron_on_off(a_account  IN VARCHAR2,
                       a_stat     IN NUMBER /* 0 vkl 1 ����*/,
                       ds         DATE,
                       de         DATE,
                       a_debugout IN NUMBER := 0) RETURN VARCHAR2 IS
    vrets VARCHAR2(1020);
    rs    NUMBER;
  BEGIN
    IF a_debugout <> '0' THEN
      dbms_output.put_line('a_account:' || a_account || ':a_stat:' ||
                           a_stat || ':ds:' || ds || ':de:' || de);
    END IF;
    IF a_stat = 0 THEN
      BEGIN
        rs := oren_wlad_srv_stat_on_off@dbsip(a_account, a_stat);
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    END IF;
    BEGIN
      vrets := oren_wlad_bron_on_off@dbsip(a_account,
                                           a_stat,
                                           trunc(ds),
                                           trunc(de) /*+1/24*23.9998*/,
                                           a_debugout);
    EXCEPTION
      WHEN OTHERS THEN
        vrets := '������ �����-IP';
    END;
    RETURN vrets;
  END;

  ----------------------------------------------------------------------------

  FUNCTION bron_on_off_iptv(p_account    VARCHAR2,
                            p_status     NUMBER,
                            p_date_start DATE,
                            p_date_end   DATE) RETURN VARCHAR2 IS
    ret VARCHAR(1024);
  BEGIN
    BEGIN
      ret := oren_mike_iptv_bron@dbsip(p_account,
                                       p_status,
                                       p_date_start,
                                       p_date_end);
    EXCEPTION
      WHEN OTHERS THEN
        ret := '������ �����-IP';
    END;
    RETURN(ret);
  END;

  ----------------------------------------------------------------------------

  PROCEDURE proc_trg_n_order_lists(usrid   NUMBER,
                                   svcid   NUMBER,
                                   numlist NUMBER,
                                   oidto   NUMBER,
                                   nidto   NUMBER,
                                   idta    NUMBER
                                   
                                   ) IS
    exis        NUMBER;
    v_res_st_ip VARCHAR2(256) := '0';
    v_acc       VARCHAR2(12);
    action      NUMBER;
    v_ds        DATE;
    v_de        DATE;
    v_svc_prov  NUMBER;
    v_init      NUMBER;
    v_blck      VARCHAR2(256);
    PROCEDURE chk_error IS
    BEGIN
      IF v_res_st_ip != '0' AND v_res_st_ip != '1' THEN
        raise_application_error(-20001,
                                '�� ������� �����/���������� �����, ��������� ����������� � �����-IP (' ||
                                v_res_st_ip || ' : ' || v_acc || ' : ' ||
                                action || ' : ' || v_ds || ' : ' || v_de || ')');
      END IF;
    END;
  
  BEGIN
    SELECT COUNT(1), account
      INTO exis, v_acc
      FROM o1.t_users u
     WHERE u.iscorp = 'N'
       AND u.user_id = usrid;
  
    SELECT COUNT(1)
      INTO v_svc_prov
      FROM msn.oren_svc_group_view v
     WHERE v.group_id = 5011737169
       AND (v.sub_group_name LIKE '%���%' OR v.sub_group_name LIKE '%���%' OR
           v.sub_group_name LIKE '%IPTV%' OR v.sub_group_name LIKE '%SIP%')
       AND v.sub_group_name NOT LIKE '%(-1)%'
       AND v.svc_id = svcid;
  
    IF exis > 0 THEN
      IF inet_bron.chksvcsect(svcid, 0) > 0 THEN
        -- ���
        inet_bron.fillsvcflags(svcid,
                               usrid,
                               numlist,
                               action,
                               v_blck,
                               v_acc,
                               v_ds,
                               v_de);
        v_res_st_ip := substr(inet_bron.bron_on_off(v_acc,
                                                    action,
                                                    v_ds,
                                                    v_de,
                                                    NULL),
                              0,
                              256);
        chk_error;
      ELSIF inet_bron.chksvcsect(svcid, 1) > 0 THEN
        -- IPTV
        inet_bron.fillsvcflags(svcid,
                               usrid,
                               numlist,
                               action,
                               v_blck,
                               v_acc,
                               v_ds,
                               v_de);
        v_res_st_ip := substr(inet_bron.bron_on_off_iptv(v_acc,
                                                         action,
                                                         v_ds,
                                                         v_de),
                              0,
                              256);
        chk_error;
      ELSIF v_svc_prov > 0 THEN
        SELECT ta.id_initiate
          INTO v_init
          FROM o1.n_titul_applic ta
         WHERE idta = ta.id_titul_applic;
        IF v_init = 1 AND oidto IS NULL AND nidto IS NOT NULL THEN
          raise_application_error(-20002,
                                  '�� ��������� ���� �������� ���������!!!');
        END IF;
      END IF;
    END IF;
  END;

----------------------------------------------------------------------------

BEGIN
  NULL;
END inet_bronb;

--==========================================================================
/
