create or replace package inet_tp is

/*
oren_wlad_get_usr_id_by_sip
oren_wlad_create_inet_zayavk
oren_wlad_get_usr_sip_svc
oren_wlad_get_TP_mainSVC



/**/

FUNCTION create_zayavk         (login IN VARCHAR2, pwd IN VARCHAR2,tp IN NUMBER) RETURN NUMBER;
FUNCTION create_zayavk_by_phn  (phon IN varchar2,tp IN NUMBER) RETURN NUMBER; ----(phon IN NUMBER,tp IN NUMBER)
FUNCTION create_zayavk_raw     (usr_idd IN NUMBER,tp IN NUMBER,conn varchar2:=null, p_from_date date := sysdate) RETURN NUMBER;


FUNCTION create_zayavk_lc(usr_id number,connname varchar2,tp IN NUMBER, p_from_date date := sysdate) RETURN varchar2;


function get_usr_sip_svc       (usr_id in NUMBER) return NUMBER;
function get_TP_mainSVC        (tp IN NUMBER) return NUMBER;
FUNCTION get_Svc_Cost_For_User (usr_id IN NUMBER,svc IN NUMBER) RETURN NUMBER;
function chk_speed             (devid in number,tfpid in number) return number;
function chk_speedUID          (usr in number,tfpid in number) return number;


function chk_CostModem         (usr in number,fsvc in number,tsvc in number) return number;

FUNCTION is_user_on_xalyava_do280809(usr_id in NUMBER) return NUMBER;
FUNCTION is_TP_on_xalyava_do280809  (TP in NUMBER) return NUMBER;


function new_task_tfp(a_account IN VARCHAR2,a_tfp_id IN NUMBER,a_date IN DATE,a_login IN VARCHAR2,
                                a_debugout IN NUMBER:=0)return NUMBER;

function new_task_tfp_msg(msg IN NUMBER)return VARCHAR2;

FUNCTION tfp_shld_run_daily RETURN NUMBER;
FUNCTION  extended_user_checks(usr IN NUMBER,tp IN NUMBER) RETURN NUMBER;


procedure update_avail_TP_cache;

end inet_tp;
/
create or replace package body inet_tp is
-----------------------------------------------------------------------------
procedure log(msg in varchar) is
begin
dbms_output.put_line(msg);
--null;
end;
-----------------------------------------------------------------------------
FUNCTION create_zayavk_by_phn  (phon IN VARCHAR2,tp IN NUMBER) RETURN NUMBER IS

BEGIN
	  RETURN create_zayavk_raw(abon_tool.user_startB(phon),tp);
END;
-----------------------------------------------------------------------------
 FUNCTION create_zayavk(login IN VARCHAR2,pwd IN VARCHAR2,tp IN NUMBER) RETURN NUMBER IS
 BEGIN
     RETURN create_zayavk_raw(abon_tool.get_usr_id_by_sip(login,pwd),tp);
 END;
-----------------------------------------------------------------------------
 FUNCTION create_zayavk_lc(usr_id number,connname varchar2,tp IN NUMBER, p_from_date date := sysdate) RETURN varchar2 IS
  ret  varchar2(1024);
 Begin
   ret:=create_zayavk_raw(usr_id,tp,connname,p_from_date);

   abon_tool.Register_action(usr_id,connname,1,abon_tool.getWRKlstItem(1,tp), new_task_tfp_msg(ret));
   abon_tool.Register_offers(usr_id,101);   

   RETURN ret||':'||new_task_tfp_msg(ret);
 End;
-----------------------------------------------------------------------------
function getTP_model(tp in number)return varchar2 is
  l_model varchar2(10):=null;
begin
  begin
      select t.model
      into l_model
      from T_TFP_INFO t
     where t.TFP_id = tp;
  exception
    when others then
      l_model := null;
  end;
    return (l_model);
End;
-----------------------------------------------------------------------------
function getTP_modelB(tp in varchar2)return varchar2 is
  l_model varchar2(10):=null;
begin
  begin
      select ucp_UC_CODE into l_model from 
      Tb_Usercateg_Plans@dbsip where ucp_tfp_code=tp;  
  exception
    when others then
      l_model := null;
  end;
    return (l_model);
End;
-----------------------------------------------------------------------------
function get_conn_sip(usr_idd IN NUMBER, tp IN NUMBER) return varchar2 is
  ret varchar2(50) := null;
  l_model varchar2(10);
  l_cnt number;
begin
  l_model:=getTP_model(tp);
  select count(*)
    into l_cnt
    from user_connections uc,
         abonent@dbsip a,
         o1.t_users u
   where uc.ab_id = a.ab_id
     and substr(a.ab_external_code, 3) = u.account
     and uc.model = l_model
     and u.user_id = usr_idd;

  if l_cnt = 1 then
    select uc.connection
      into ret
      from user_connections uc, abonent@dbsip a, o1.t_users u
     where uc.ab_id = a.ab_id
       and substr(a.ab_external_code, 3) = u.account
       and uc.model = l_model
       and u.user_id = usr_idd
       and rownum = 1;
  end if;

  return (ret);
exception
  when others then
    ret := null;
    return (ret);
end;
-----------------------------------------------------------------------------
function get_tfp_line_svc(p_tfp_id number) return number is
  l_ret number;
begin
  begin
    select line_svc_id
      into l_ret
      from t_tfp_info
     where tfp_id = p_tfp_id;
  exception 
    when no_data_found then
      l_ret := -1;
  end;        
  return (l_ret);
end;
-----------------------------------------------------------------------------
function get_usr_sip_dev_id(p_user_id number, p_svc_id number) return number is
  ret number;
begin
  begin
    select dev_id
      into ret
      from o1.t_services s
     where s.user_id = p_user_id
       and s.svc_id = p_svc_id
       and nvl(s.date_end, sysdate + 1) > sysdate;
  exception
    when no_data_found then
      ret := -1;
  end;
  return ret;  
end;
-----------------------------------------------------------------------------
function get_tfp_old_line_svc(p_dev_id number) return number is
  l_ret number;
begin
  begin
    select s.svc_id
      into l_ret
      from o1.t_services s, o1.t_svc_ref sr
     where s.svc_id = sr.svc_id
       and s.dev_id = p_dev_id
       and sr.svctype = 0
       and sr.equip_type_id = 'P'
       and sysdate between s.date_begin and nvl(s.date_end, sysdate + 1);
  exception
    when no_data_found then
      l_ret := -1;
  end; 
  return (l_ret);
end;
-----------------------------------------------------------------------------
function checkConnectionModel(p_user_id number, p_new_tp number, p_conn varchar2) return number
is
  l_ret number;
  mdl varchar2(10);
begin
  mdl:=getTP_model(p_new_tp);
  select count(*)
    into l_ret
    from user_connections uc, abonent@dbsip a, o1.t_abs_users au
   where uc.ab_id = a.ab_id
     and a.ab_external_code = au.abs_id
     and au.user_id = p_user_id
     and uc.connection = p_conn
     and uc.model =mdl;
  return (l_ret);
end;
-----------------------------------------------------------------------------
function checkConnectionTP(tpp in number, conn in varchar2) return number is
  l_ret number;
begin
  select count(1) into l_ret from USER_CONNECTIONS t where t.connection=conn and t.tp=tpp;
  if l_ret>0 then
    return 0;
  end if;
  return 1;
end;
-----------------------------------------------------------------------------
function hasTodayZayavk(p_user_id number,conn varchar2) return number
is
  l_ret number;
begin
  select decode(count(*), 0, 0, 1)
    into l_ret
    from site_jrnl j
   where j.user_id = p_user_id
     and j.dev=conn
     and trunc(sysdate) = trunc(j.cdate)
     and j.ztype = 1
     and j.ztextout = '������� ����������';
  return (l_ret);
end;
-----------------------------------------------------------------------------
FUNCTION create_zayavk_raw_notdo(usr_idd IN NUMBER,tp IN NUMBER,conn varchar2:=null, p_from_date date := sysdate) RETURN NUMBER is
   res          number;
   cc           number;
begin
    log('usr_id: ' || usr_idd);
    if conn is not null then
      select count(1) into cc from o1.v_instlmnt where isactiv='Y' and svc_id in(select svc_id FROM INET_USR_SVC_BY_MSN WHERE user_id=usr_idd) and user_id=usr_idd;
      if cc=0 then
  
        res:=        new_task_tfp(null,--not used in corrent release if function
                                  tp,p_from_date,conn);
      else          res := -5;  end if;
    else          res := -13;  end if;
    return(res);   
end;
-----------------------------------------------------------------------------
function create_zayavk_raw(usr_idd in number, tp in number, conn varchar2 := null, p_from_date date := sysdate) return number is
  res          number;
  res_ph       number := 1;
  usr          number;
  fsvc         number;
  tsvc         number;
  new_line_svc number;
  old_line_svc number;  
  l_dev_id     number := 1;
  l_conn       varchar2(50);
begin
  res := 0;
  log('usr_id: ' || usr_idd);
  if usr_idd > 0 then
    if hasTodayZayavk(usr_idd,conn) = 0 then
      if checkConnectionModel(usr_idd, tp, conn) > 0 then
        usr := extended_user_checks(usr_idd, tp);
        log('extended_user_checks: ' || usr);
        if usr > 0 then
          if checkConnectionTP(tp, conn)>0 then
                      if conn is null then
                        l_conn := get_conn_sip(usr_idd, tp);
                      else
                        l_conn := conn;
                      end if;            
            fsvc := get_usr_sip_svc(usr);
            log('fsvc: ' || fsvc);
            if fsvc > 0 then
              tsvc := get_TP_mainSVC(tp);
              log('tsvc: ' || tsvc);
              if tsvc > 0 then
--                if tsvc <> fsvc then
                  if chk_CostModem(usr, fsvc, tsvc) > 0 then
                    -- ���� ������ ��� ��� �����
                    new_line_svc := get_tfp_line_svc(p_tfp_id => tp);
                    if new_line_svc != -1 then
                      l_dev_id := get_usr_sip_dev_id(p_user_id => usr, p_svc_id => fsvc);
                      if l_dev_id > 0 then
                        old_line_svc := get_tfp_old_line_svc(p_dev_id => l_dev_id);
                        -- ���� ������ �� ��������� � ������ ����� ����, ������ ���. �����
                        if new_line_svc != old_line_svc and old_line_svc > -1 then
                          res_ph := zayvl_api.get_applic_smena_102(p_raz_svc_id  => 40270779,
                                                                   p_user_id     => usr,
                                                                   p_dev_id      => l_dev_id,
                                                                   p_new_svc_id  => new_line_svc,
                                                                   p_date_create => p_from_date);
                        end if;
                      end if;
                      log('res:' || res_ph);
                    end if;
                    if l_dev_id > 0 and res_ph > 0 then
                      res := zayvl_api.get_applic_smena_75(user_id       => usr,
                                                           raz_svc_id    => 22131039,
                                                           old_svc_id    => fsvc,
                                                           new_svc_id    => tsvc,
                                                           dev_id        => null,
                                                           p_date_create => p_from_date);
                      log('res:' || res);
                      if res > 0 and l_conn is not null then
                        insert into t_zayvk_subject (num_aplic, subject, tfp_id) values (res, l_conn, tp);
                      end if;
                    end if;
                  end if;
              else        res := tsvc; end if;
            else
              if fsvc=-5 then
                      res :=create_zayavk_raw_notdo(usr_idd,tp,conn,p_from_date);
                else      res := fsvc; end if;
            end if;
          else          res := -10;  end if;
        else            res := usr;  end if;
      else              res := -40;  end if;
    else                res := -50;  end if;
  else                  res := usr_idd; end if;
  return(res);
end;
-----------------------------------------------------------------------------
function get_usr_sip_svc(usr_id in NUMBER) return number is
  r number;
BEGIN
  SELECT count(1) INTO r FROM INET_USR_SVC_BY_MSN WHERE user_id=usr_id;
	IF r=1 THEN
    SELECT svc_id INTO r FROM INET_USR_SVC_BY_MSN WHERE user_id=usr_id;
	ELSE
   IF r<1 THEN
	   r:=-4;
	 ELSE
	   r:=-5;
		END IF;
	END IF;
  return(r);
end get_usr_sip_svc;
-----------------------------------------------------------------------------
function chk_CostModem         (usr in number,fsvc in number,tsvc in number) return number is
  fCost  NUMBER;
  tCost  NUMBER;
  ncCost NUMBER;
  dd date; -- ������ �.�.

begin
            --������ �.�. �������� � �� ���������� �� �����? (���� ������� ����� - ����� ������ �������� ����!!!))
            --SELECT COUNT(1) INTO ncCost FROM o1.t_svc_ref WHERE svc_id=fsvc AND upper(NAME) LIKE '%�����%' AND upper(NAME) NOT LIKE '%��� �����%';
/*            SELECT COUNT(1)
              INTO ncCost
              from o1.t_services s,
                   (select lb.svc_id, lc.note
                      from o1.t_svc_layer_cell lc, o1.t_svc_layer_bunch lb
                     where lc.id_cell_svc = lb.id_cell_svc
                       and lc.id_layer_svc = 6251229851
                       and lc.note is not null) r
             where 2 = 2
               and r.svc_id = s.svc_id
               and s.svc_id = fsvc
               and s.user_id = usr
               and s.date_end is null
               and s.date_begin < to_date(sysdate, 'DD.MM.RRRR')
               and add_months(s.date_begin, r.note) >
                   to_date(sysdate, 'DD.MM.RRRR');*/
            --�������� ���������  
                                   
            -- ������ �.�.
            select case
                   when min(date_begin) is null then
                     sysdate
                   else
                     min(date_begin)
                   end k
               into dd
               from o1.t_services s
              where s.svc_id in (select lb.svc_id
                                   from o1.t_svc_layer_cell lc, o1.t_svc_layer_bunch lb
                                  where lc.id_cell_svc = lb.id_cell_svc
                                    and lc.id_layer_svc = 6251229851
                                    and lc.note is not null)
                and s.user_id = usr;     
                
              SELECT COUNT(1)
                INTO ncCost
                from o1.t_services s,
                   (select lb.svc_id, lc.note
                      from o1.t_svc_layer_cell lc, o1.t_svc_layer_bunch lb
                     where lc.id_cell_svc = lb.id_cell_svc
                       and lc.id_layer_svc = 6251229851
                       and lc.note is not null) r
              where 2 = 2
                and r.svc_id = s.svc_id
                and s.svc_id = fsvc
                and s.user_id = usr
                and s.date_end is null
                and s.date_begin < to_date(sysdate, 'DD.MM.RRRR')
                and add_months(dd, r.note) > to_date(sysdate, 'DD.MM.RRRR');
            -- �����
          
            log('ncCost: ' || ncCost);
            IF ncCost > 0 THEN    fCost := get_Svc_Cost_For_User(usr, fsvc);log('fCost: ' || fCost);
              IF fCost >= 0 THEN  tCost := get_Svc_Cost_For_User(usr, tsvc);log('tCost: ' || tCost);
                IF tCost >= 0 THEN                               log('tCost > 0: true');
                  IF fCost < tCost THEN                         log('fCost < tCost : true');
                  
                  ELSE                                           log('fCost < tCost : false');
                    return -9;
                  END IF;
                ELSE     return -8;      END IF;
              ELSE       return -7;      END IF;
            END IF;
            return 1;
end;
-----------------------------------------------------------------------------
function get_TP_mainSVC(tp IN NUMBER) return number is
  Result number;

BEGIN
  BEGIN
    /*
SELECT tsr.svc_id INTO Result
  FROM msn.oren_tfp_cross otc,
       msn.oren_tfp_ref otr,
       o1.t_svc_ref tsr
 WHERE otc.group_id=5 
   AND otc.tfp_id=otr.tfp_id
	 AND otc.svc_id=tsr.svc_id
   AND tsr.isconst='Y' 
	 AND tsr.seldom_used IS NULL
	 AND otr.tfp_id=tp
	 AND otc.svc_id IN (SELECT svc_id FROM o1.t_svc_layer_bunch WHERE  id_layer_svc=5926589644 AND id_cell_svc=5926590210); 
  */
        select svc_id into result from t_tfp_info where tfp_id = tp;
       
  exception
    WHEN others THEN
      Result:=-6;
  END;
RETURN (Result);
end get_TP_mainSVC;
-----------------------------------------------------------------------------
FUNCTION get_Svc_Cost_For_User(usr_id IN NUMBER,svc IN NUMBER) RETURN NUMBER IS
 res NUMBER;
BEGIN
  BEGIN
    select svr.value INTO res from o1.t_svc_tarif_ref svr
      where 1=1
         and svr.svc_id=svc
         and svr.user_type_id IN (SELECT u.user_type_id FROM o1.t_users u WHERE user_id=usr_id)
         and svr.date_begin=(select max(svr1.date_begin) from o1.t_svc_tarif_ref svr1
                                            where svr.svc_id=svr1.svc_id
                                            and svr1.user_type_id=svr.user_type_id
                                            AND svr1.date_begin<=sysdate
                                          );
	EXCEPTION
	WHEN OTHERS tHEN
	  res:=-999999;
  END;
RETURN (res);
END get_Svc_Cost_For_User;
-----------------------------------------------------------------------------
FUNCTION is_user_on_xalyava_do280809(usr_id in NUMBER) return NUMBER IS
res NUMBER;
BEGIN
SELECT COUNT(1) INTO res FROM o1.t_streets_ref str 

WHERE str.street_id IN
(SELECT street_id FROM o1.o_houses WHERE house_id IN( SELECT house_id FROM o1.t_users WHERE user_id=usr_id))
AND str.town_id IN (
33,22,70,23,836570796,836570725,4,12,41,45,
32,300241432,300241383,300339491,836655917,276002,276003,303060606,1383421218,276004,836656104,
34,836747041,836747169,6170227410,3141856373,252007,270883,263074,263077,270928,
300339741,263053,836747336);

RETURN (res);
END;

-----------------------------------------------------------------------------
FUNCTION is_TP_on_xalyava_do280809  (TP in NUMBER) return NUMBER IS

BEGIN
 IF tp=102 OR tp=103 OR tp=104 THEN
  RETURN 1;
	ELSE RETURN 0;
	END IF;
END;

-----------------------------------------------------------------------------
function new_task_tfp
(a_account IN VARCHAR2,
a_tfp_id IN NUMBER,
a_date IN DATE,
a_login IN VARCHAR2,
a_debugout IN NUMBER:=0)
return number is
begin
  return oren_wlad_cctp@dbsip(a_login,a_tfp_id,a_date,a_debugout);
--  return(oren_wlad_new_task_tfp@dbsip(a_account,a_tfp_id,a_date,a_login,a_debugout));
END;
-----------------------------------------------------------------------------
function new_task_tfp_msg(msg IN NUMBER)
return VARCHAR2 is
BEGIN

 IF msg<0 THEN
   IF msg=- 1 THEN RETURN '������ � ���������� ������';END IF;
   IF msg=- 2 THEN RETURN '������ ��������� �������������� �������� � �����-IP';END IF;
   IF msg=- 3 THEN RETURN '������ ��������� ������ �����������';END IF;
   IF msg=- 4 THEN RETURN '������ ��������� ������ ����������� � �������������� ������';END IF;
   IF msg=- 5 THEN RETURN '������ ��������� ���������� �������������� ��';END IF;
   IF msg=- 6 THEN RETURN '������ ������������� �� ����� ���������� ����������, ������ 1�� �� ';END IF;
   IF msg=- 7 THEN RETURN '������ ������������� �� ����� ���������� ���������� ������ ����';END IF;
   IF msg=- 8 THEN RETURN '������ ��������� ����� ��';END IF;
   IF msg=- 9 THEN RETURN '������ ������ ����� ������� �� ��� �������� �� ������ ������';END IF;
   IF msg=-10 THEN RETURN '������ ��������� �� ��� �������';END IF;	 	 
   IF msg=-15 THEN RETURN '������ ������ ������������ ��������� �� � �������������� ������';END IF;
   if msg=-40 then return '������ ������ ������������ ������ �������� ����, ��� ��� �� ������������ ��� ������ ����������'; end if;
   if msg=-50 then return '������. ��������� ��������� �� ����� ��������� ����� ����� �� ����� ������ ���� � �����'; end if;
   IF msg=-777 THEN RETURN '������ ������ ������������ ��������� �� � ������� ��������';END IF;   
   if msg=-13 then RETURN '������ ������������� �����������.';END IF;   
   return msg;
  ELSE
	RETURN '������� ����������';
	END IF;
END;
-----------------------------------------------------------------------------
function tfp_shld_run_daily return number is
  seq    number;
  uidres number;
begin
  select TFP_SHLD_SEQ.NEXTVAL into seq from dual;
  for x in (select U.USER_ID,
                   U.ACCOUNT,
                   nvl(zs.tfp_id,-1) TFP_ID,
                   TO_DATE(LK.DATE_CREATE, 'DD.MM.RRRR') A_DATE,
                   lk.rowid rd,
                   zs.subject
              from MSN.OREN_LICHNIY_KABINET LK
              left join t_zayvk_subject zs on lk.num_applic = zs.num_aplic, O1.T_USERS U, o1.n_applic_life oo
             where LK.TIP = 5
               and LK.IS_DEL = 2
               and LK.ACTION_NAR is not null
               and LK.STARTIP is null
               and LK.REZ = 'OK'
               and LK.USER_ID = U.USER_ID
                  --   AND LK.NEW_SVC_ID IN (6193826420, 6193826695, 6193827228)    -- ��� �����
               and lk.num_list = oo.num_list
               and oo.type_action_id = 1
             order by lk.user_id, oo.date_sys)
  loop
    begin
      if x.tfp_id>0 then
      --      uidres:=inet_tp.new_task_tfp(x.account,x.tfp_id,x.a_date+1,null);
      uidres := inet_tp.new_task_tfp(x.account, x.tfp_id, trunc(x.a_date) + 1, x.subject);
      else
        uidres:=-778;
      end if;
      update MSN.OREN_LICHNIY_KABINET l set l.startip = uidres where l.rowid = x.rd;
    
      if uidres < 0 then
        mail.sendMail_single3('dba@orenburg.vt.ru',
                              'inet_tp.tfp_shld_run_daily',
                              'USER_ID: ' || x.USER_ID || '<br>' || 'account: ' || x.account || '<br>' || 'tfp_id:  ' || x.tfp_id ||
                              '<br>' || 'uidres: ' || uidres || '<br>');
      end if;
    exception
      when others then
        if uidres is null then
          uidres := -10;
        end if;
        mail.sendMail_single3('dba@orenburg.vt.ru',
                              'inet_tp.tfp_shld_run_daily',
                              'USER_ID: ' || x.USER_ID || '<br>' || 'account: ' || x.account || '<br>' || 'tfp_id:  ' || x.tfp_id ||
                              '<br>' || 'uidres: ' || uidres || '<br>');
        /**/
    end;
  end loop;
  --dbms_output.put_line('shld seq IS '||seq);
  return seq;
end;
-----------------------------------------------------------------------------
function chk_speed(devid in number,tfpid in number) return number is
  spp number;
  tpspeed number;
  mdl varchar2(10);
begin
  begin
    select speed into tpspeed from t_tfp_info  where tfp_id=tfpid;
    exception
      when others then
        tpspeed:=1024;
  end;
  log('tpspeed: '||tpspeed);  
  mdl:=getTP_model(tfpid);
  log('model: '||mdl);
  if mdl='HwFT�' then
    spp:=999999999;
    tpspeed:=1024;
      log('spp: '||spp);
  else
  
  begin
    select (speed*1024)/(ports*0.6)  into spp
      from oren_ats_speed
     where ats_id in(select dept_id
                       from o1.t_volume tv
                      where tv.dev_id=devid);
    exception
      when others then
        spp:=999999999;
  end;
  log('spp: '||spp);
  end if;
  if tpspeed<spp then
    spp:=1;
  else
    spp:=0;
  end if;
  log('res: '||spp);
  return(spp);
end;
-----------------------------------------------------------------------------
function chk_speedUID(usr in number,tfpid in number) return number is
dev number;
begin
  begin
    select max(dev_id) into dev from o1.t_services where user_id=usr and date_end is null;
    exception
    when others then dev:=0;
  end;
  return  chk_speed(dev,tfpid);
end;
-----------------------------------------------------------------------------
FUNCTION extended_user_checks(usr IN NUMBER,tp IN NUMBER) RETURN NUMBER IS
BEGIN
		IF is_TP_on_xalyava_do280809(tp)>0 THEN
		  IF is_user_on_xalyava_do280809(usr)=0 THEN
			  RETURN (-777);
			END IF;
		END IF;
    if chk_speedUID(usr,tp)=0 then
     return -15;
    end if;
		RETURN(usr);
END;
-----------------------------------------------------------------------------
procedure update_avail_TP_cache is
begin
delete from AVAIL_INET_TP_CACHE;
insert into AVAIL_INET_TP_CACHE value(
select ctuc.uc_code,tbtp.tfp_code,tbtp.tfp_name
  FROM TB_USERCATEG_PLANS@dbsip tbup,
       CT_USER_CATEGORYS@dbsip  ctuc,
       TB_TARIFF_PLANS@dbsip    tbtp,
       T_TFP_INFO               tfp
where  1=1
   and tbup .ucp_uc_code  = ctuc.uc_code
   and tbup .ucp_tfp_code = tbtp.tfp_code
   and tbup .ucp_tfp_code = tfp .tfp_code
   and tfp.tfp_code       = tbtp.tfp_code
   and tfp.model          = ctuc.uc_code
);
commit;
end;
-----------------------------------------------------------------------------
begin
  -- Initialization
  NULL;
end inet_tp;
/
