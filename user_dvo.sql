create or replace view user_dvo as
select "ID","NAME","ISON","SRVID","CH_STATE","CH_DATE","USER_ID","DEV_ID","SERVICE_ID" from (
select udl.id,udl.name,
       nvl (ss.ison,0) as ison,
       case nvl(nmn.service_id, 0) when 0 then 0 else 1 end srvid,nvl(udw.state,0) ch_state,
       to_char(nvl(udw.date_post,sysdate),'dd.mm.yyyy') as ch_date ,udl.user_id,udl.dev_id,nvl(nmn.service_id, 0) as service_id
  from (select lcw.id,lcw.name,s.dev_id,s.user_id,lcw.c_svc_gtsnm,lcw.c_svc_stsnm,lcw.c_svc_sts,lcw.c_svc_gts from
           (select s.dev_id,s.user_id from o1.t_services s group by s.dev_id,s.user_id) s,
            t_LC_DVO_SVC lcw
       ) udl
  left join
       (
        select dev_id,id,count(1) ison from(
        (select v.dev_id,ldc.id from
               v_lc_dvo_svc ldc
          join o1.t_svc_ref  sr  on sr.svc_id=ldc.CSG
          join o1.tu_ats_dvo ad  on ad.svc_id=sr.svc_id
                                and sr.seldom_used is null
          join O1.T_ATS      A   on A.ATS_ID=ad.ats_id
          join O1.T_VOLUME   v   on V.DEPT_ID = A.ATS_ID
          where UPPER(A.COMMENTS) LIKE '%C��%'
          group by v.dev_id,ldc.id) union
        (select v.dev_id,ldc.id from
               v_lc_dvo_svc ldc
          join o1.t_svc_ref  sr  on sr.svc_id=ldc.CSG
          join o1.tu_ats_dvo ad  on ad.svc_id=sr.svc_id
                                and sr.seldom_used is null
          join O1.T_ATS      A   on A.ATS_ID=ad.ats_id
          join O1.T_VOLUME   v   on V.DEPT_ID = A.ATS_ID
          where UPPER(A.COMMENTS) LIKE '%���%'
          group by v.dev_id,ldc.id))
        group by dev_id,id) ss on ss.id=udl.id and ss.dev_id=udl.dev_id

         LEFT JOIN o1.t_services nmn
                ON nmn.dev_id=(udl.dev_id)
               AND nmn.user_id=(udl.user_id)
               AND nmn.svc_id in (udl.c_svc_gtsnm,udl.c_svc_stsnm,udl.c_svc_sts,udl.c_svc_gts)
               AND nmn.date_end IS NULL
         left join USERS_DVO_WANTS udw on udw.user_id=(udl.user_id)
               and udw.dev_id=(udl.dev_id)
               and udl.id=udw.dvo_id
--               where udl.user_id=72785 and udl.dev_id=3532560588
--              where udl.user_id=232000073 and udl.dev_id=3533226200


         order by udl.id
    );
